
#include "wci/intermediate/TypeSpec.h"
using namespace wci::intermediate;


// Generated from Rat.g4 by ANTLR 4.7.2


#include "RatVisitor.h"

#include "RatParser.h"


using namespace antlrcpp;
using namespace antlr4;

RatParser::RatParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

RatParser::~RatParser() {
  delete _interpreter;
}

std::string RatParser::getGrammarFileName() const {
  return "Rat.g4";
}

const std::vector<std::string>& RatParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& RatParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

RatParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::HeaderContext* RatParser::ProgramContext::header() {
  return getRuleContext<RatParser::HeaderContext>(0);
}

RatParser::MainBlockContext* RatParser::ProgramContext::mainBlock() {
  return getRuleContext<RatParser::MainBlockContext>(0);
}


size_t RatParser::ProgramContext::getRuleIndex() const {
  return RatParser::RuleProgram;
}


antlrcpp::Any RatParser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ProgramContext* RatParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, RatParser::RuleProgram);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(70);
    header();
    setState(71);
    mainBlock();
    setState(72);
    match(RatParser::T__0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HeaderContext ------------------------------------------------------------------

RatParser::HeaderContext::HeaderContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::HeaderContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::HeaderContext::getRuleIndex() const {
  return RatParser::RuleHeader;
}


antlrcpp::Any RatParser::HeaderContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitHeader(this);
  else
    return visitor->visitChildren(this);
}

RatParser::HeaderContext* RatParser::header() {
  HeaderContext *_localctx = _tracker.createInstance<HeaderContext>(_ctx, getState());
  enterRule(_localctx, 2, RatParser::RuleHeader);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(74);
    match(RatParser::T__1);
    setState(75);
    match(RatParser::IDENTIFIER);
    setState(76);
    match(RatParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MainBlockContext ------------------------------------------------------------------

RatParser::MainBlockContext::MainBlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::BlockContext* RatParser::MainBlockContext::block() {
  return getRuleContext<RatParser::BlockContext>(0);
}


size_t RatParser::MainBlockContext::getRuleIndex() const {
  return RatParser::RuleMainBlock;
}


antlrcpp::Any RatParser::MainBlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitMainBlock(this);
  else
    return visitor->visitChildren(this);
}

RatParser::MainBlockContext* RatParser::mainBlock() {
  MainBlockContext *_localctx = _tracker.createInstance<MainBlockContext>(_ctx, getState());
  enterRule(_localctx, 4, RatParser::RuleMainBlock);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(78);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

RatParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::BlockContext::TRIGGER() {
  return getToken(RatParser::TRIGGER, 0);
}

RatParser::StmtListContext* RatParser::BlockContext::stmtList() {
  return getRuleContext<RatParser::StmtListContext>(0);
}

tree::TerminalNode* RatParser::BlockContext::PULLOUT() {
  return getToken(RatParser::PULLOUT, 0);
}

RatParser::DeclarationsContext* RatParser::BlockContext::declarations() {
  return getRuleContext<RatParser::DeclarationsContext>(0);
}

std::vector<RatParser::ProcDeclContext *> RatParser::BlockContext::procDecl() {
  return getRuleContexts<RatParser::ProcDeclContext>();
}

RatParser::ProcDeclContext* RatParser::BlockContext::procDecl(size_t i) {
  return getRuleContext<RatParser::ProcDeclContext>(i);
}

std::vector<RatParser::FuncDeclContext *> RatParser::BlockContext::funcDecl() {
  return getRuleContexts<RatParser::FuncDeclContext>();
}

RatParser::FuncDeclContext* RatParser::BlockContext::funcDecl(size_t i) {
  return getRuleContext<RatParser::FuncDeclContext>(i);
}


size_t RatParser::BlockContext::getRuleIndex() const {
  return RatParser::RuleBlock;
}


antlrcpp::Any RatParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

RatParser::BlockContext* RatParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 6, RatParser::RuleBlock);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(81);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == RatParser::IDS) {
      setState(80);
      declarations();
    }
    setState(86);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::PROC) {
      setState(83);
      procDecl();
      setState(88);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(92);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::FUNC) {
      setState(89);
      funcDecl();
      setState(94);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(95);
    match(RatParser::TRIGGER);
    setState(96);
    stmtList();
    setState(97);
    match(RatParser::PULLOUT);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclarationsContext ------------------------------------------------------------------

RatParser::DeclarationsContext::DeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::DeclarationsContext::IDS() {
  return getToken(RatParser::IDS, 0);
}

RatParser::DecListContext* RatParser::DeclarationsContext::decList() {
  return getRuleContext<RatParser::DecListContext>(0);
}


size_t RatParser::DeclarationsContext::getRuleIndex() const {
  return RatParser::RuleDeclarations;
}


antlrcpp::Any RatParser::DeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitDeclarations(this);
  else
    return visitor->visitChildren(this);
}

RatParser::DeclarationsContext* RatParser::declarations() {
  DeclarationsContext *_localctx = _tracker.createInstance<DeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 8, RatParser::RuleDeclarations);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(99);
    match(RatParser::IDS);
    setState(100);
    decList();
    setState(101);
    match(RatParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DecListContext ------------------------------------------------------------------

RatParser::DecListContext::DecListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::DeclContext *> RatParser::DecListContext::decl() {
  return getRuleContexts<RatParser::DeclContext>();
}

RatParser::DeclContext* RatParser::DecListContext::decl(size_t i) {
  return getRuleContext<RatParser::DeclContext>(i);
}


size_t RatParser::DecListContext::getRuleIndex() const {
  return RatParser::RuleDecList;
}


antlrcpp::Any RatParser::DecListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitDecList(this);
  else
    return visitor->visitChildren(this);
}

RatParser::DecListContext* RatParser::decList() {
  DecListContext *_localctx = _tracker.createInstance<DecListContext>(_ctx, getState());
  enterRule(_localctx, 10, RatParser::RuleDecList);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(103);
    decl();
    setState(108);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(104);
        match(RatParser::T__2);
        setState(105);
        decl(); 
      }
      setState(110);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclContext ------------------------------------------------------------------

RatParser::DeclContext::DeclContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::VarListContext* RatParser::DeclContext::varList() {
  return getRuleContext<RatParser::VarListContext>(0);
}

RatParser::TypeIdContext* RatParser::DeclContext::typeId() {
  return getRuleContext<RatParser::TypeIdContext>(0);
}


size_t RatParser::DeclContext::getRuleIndex() const {
  return RatParser::RuleDecl;
}


antlrcpp::Any RatParser::DeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitDecl(this);
  else
    return visitor->visitChildren(this);
}

RatParser::DeclContext* RatParser::decl() {
  DeclContext *_localctx = _tracker.createInstance<DeclContext>(_ctx, getState());
  enterRule(_localctx, 12, RatParser::RuleDecl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(111);
    varList();
    setState(112);
    match(RatParser::T__3);
    setState(113);
    typeId();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VarListContext ------------------------------------------------------------------

RatParser::VarListContext::VarListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::VarIdContext *> RatParser::VarListContext::varId() {
  return getRuleContexts<RatParser::VarIdContext>();
}

RatParser::VarIdContext* RatParser::VarListContext::varId(size_t i) {
  return getRuleContext<RatParser::VarIdContext>(i);
}


size_t RatParser::VarListContext::getRuleIndex() const {
  return RatParser::RuleVarList;
}


antlrcpp::Any RatParser::VarListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitVarList(this);
  else
    return visitor->visitChildren(this);
}

RatParser::VarListContext* RatParser::varList() {
  VarListContext *_localctx = _tracker.createInstance<VarListContext>(_ctx, getState());
  enterRule(_localctx, 14, RatParser::RuleVarList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(115);
    varId();
    setState(120);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::T__4) {
      setState(116);
      match(RatParser::T__4);
      setState(117);
      varId();
      setState(122);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VarIdContext ------------------------------------------------------------------

RatParser::VarIdContext::VarIdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::VarIdContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::VarIdContext::getRuleIndex() const {
  return RatParser::RuleVarId;
}


antlrcpp::Any RatParser::VarIdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitVarId(this);
  else
    return visitor->visitChildren(this);
}

RatParser::VarIdContext* RatParser::varId() {
  VarIdContext *_localctx = _tracker.createInstance<VarIdContext>(_ctx, getState());
  enterRule(_localctx, 16, RatParser::RuleVarId);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(123);
    match(RatParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeIdContext ------------------------------------------------------------------

RatParser::TypeIdContext::TypeIdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::TypeIdContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::TypeIdContext::getRuleIndex() const {
  return RatParser::RuleTypeId;
}


antlrcpp::Any RatParser::TypeIdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitTypeId(this);
  else
    return visitor->visitChildren(this);
}

RatParser::TypeIdContext* RatParser::typeId() {
  TypeIdContext *_localctx = _tracker.createInstance<TypeIdContext>(_ctx, getState());
  enterRule(_localctx, 18, RatParser::RuleTypeId);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(125);
    match(RatParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcDeclContext ------------------------------------------------------------------

RatParser::ProcDeclContext::ProcDeclContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::ProcDeclContext::PROC() {
  return getToken(RatParser::PROC, 0);
}

RatParser::MethodIdContext* RatParser::ProcDeclContext::methodId() {
  return getRuleContext<RatParser::MethodIdContext>(0);
}

tree::TerminalNode* RatParser::ProcDeclContext::TRIGGER() {
  return getToken(RatParser::TRIGGER, 0);
}

tree::TerminalNode* RatParser::ProcDeclContext::PULLOUT() {
  return getToken(RatParser::PULLOUT, 0);
}

RatParser::ParamListContext* RatParser::ProcDeclContext::paramList() {
  return getRuleContext<RatParser::ParamListContext>(0);
}

RatParser::StmtListContext* RatParser::ProcDeclContext::stmtList() {
  return getRuleContext<RatParser::StmtListContext>(0);
}


size_t RatParser::ProcDeclContext::getRuleIndex() const {
  return RatParser::RuleProcDecl;
}


antlrcpp::Any RatParser::ProcDeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitProcDecl(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ProcDeclContext* RatParser::procDecl() {
  ProcDeclContext *_localctx = _tracker.createInstance<ProcDeclContext>(_ctx, getState());
  enterRule(_localctx, 20, RatParser::RuleProcDecl);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(127);
    match(RatParser::PROC);
    setState(128);
    methodId();
    setState(129);
    match(RatParser::T__5);
    setState(131);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == RatParser::IDENTIFIER) {
      setState(130);
      paramList();
    }
    setState(133);
    match(RatParser::T__6);
    setState(134);
    match(RatParser::T__2);
    setState(135);
    match(RatParser::TRIGGER);
    setState(137);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      setState(136);
      stmtList();
      break;
    }

    }
    setState(139);
    match(RatParser::PULLOUT);
    setState(140);
    match(RatParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncDeclContext ------------------------------------------------------------------

RatParser::FuncDeclContext::FuncDeclContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::FuncDeclContext::FUNC() {
  return getToken(RatParser::FUNC, 0);
}

RatParser::MethodIdContext* RatParser::FuncDeclContext::methodId() {
  return getRuleContext<RatParser::MethodIdContext>(0);
}

RatParser::TypeIdContext* RatParser::FuncDeclContext::typeId() {
  return getRuleContext<RatParser::TypeIdContext>(0);
}

tree::TerminalNode* RatParser::FuncDeclContext::TRIGGER() {
  return getToken(RatParser::TRIGGER, 0);
}

tree::TerminalNode* RatParser::FuncDeclContext::PULLOUT() {
  return getToken(RatParser::PULLOUT, 0);
}

RatParser::ParamListContext* RatParser::FuncDeclContext::paramList() {
  return getRuleContext<RatParser::ParamListContext>(0);
}

RatParser::StmtListContext* RatParser::FuncDeclContext::stmtList() {
  return getRuleContext<RatParser::StmtListContext>(0);
}

tree::TerminalNode* RatParser::FuncDeclContext::RETURN() {
  return getToken(RatParser::RETURN, 0);
}

RatParser::ExprContext* RatParser::FuncDeclContext::expr() {
  return getRuleContext<RatParser::ExprContext>(0);
}


size_t RatParser::FuncDeclContext::getRuleIndex() const {
  return RatParser::RuleFuncDecl;
}


antlrcpp::Any RatParser::FuncDeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitFuncDecl(this);
  else
    return visitor->visitChildren(this);
}

RatParser::FuncDeclContext* RatParser::funcDecl() {
  FuncDeclContext *_localctx = _tracker.createInstance<FuncDeclContext>(_ctx, getState());
  enterRule(_localctx, 22, RatParser::RuleFuncDecl);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(142);
    match(RatParser::FUNC);
    setState(143);
    methodId();
    setState(144);
    match(RatParser::T__5);
    setState(146);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == RatParser::IDENTIFIER) {
      setState(145);
      paramList();
    }
    setState(148);
    match(RatParser::T__6);
    setState(149);
    match(RatParser::T__3);
    setState(150);
    typeId();
    setState(151);
    match(RatParser::T__2);
    setState(152);
    match(RatParser::TRIGGER);
    setState(154);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx)) {
    case 1: {
      setState(153);
      stmtList();
      break;
    }

    }
    setState(160);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == RatParser::RETURN) {
      setState(156);
      match(RatParser::RETURN);
      setState(157);
      expr(0);
      setState(158);
      match(RatParser::T__2);
    }
    setState(162);
    match(RatParser::PULLOUT);
    setState(163);
    match(RatParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MethodIdContext ------------------------------------------------------------------

RatParser::MethodIdContext::MethodIdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::MethodIdContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::MethodIdContext::getRuleIndex() const {
  return RatParser::RuleMethodId;
}


antlrcpp::Any RatParser::MethodIdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitMethodId(this);
  else
    return visitor->visitChildren(this);
}

RatParser::MethodIdContext* RatParser::methodId() {
  MethodIdContext *_localctx = _tracker.createInstance<MethodIdContext>(_ctx, getState());
  enterRule(_localctx, 24, RatParser::RuleMethodId);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(165);
    match(RatParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamListContext ------------------------------------------------------------------

RatParser::ParamListContext::ParamListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::DeclContext *> RatParser::ParamListContext::decl() {
  return getRuleContexts<RatParser::DeclContext>();
}

RatParser::DeclContext* RatParser::ParamListContext::decl(size_t i) {
  return getRuleContext<RatParser::DeclContext>(i);
}


size_t RatParser::ParamListContext::getRuleIndex() const {
  return RatParser::RuleParamList;
}


antlrcpp::Any RatParser::ParamListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitParamList(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ParamListContext* RatParser::paramList() {
  ParamListContext *_localctx = _tracker.createInstance<ParamListContext>(_ctx, getState());
  enterRule(_localctx, 26, RatParser::RuleParamList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(167);
    decl();
    setState(172);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::T__7) {
      setState(168);
      match(RatParser::T__7);
      setState(169);
      decl();
      setState(174);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StmtContext ------------------------------------------------------------------

RatParser::StmtContext::StmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::DeclarationsContext* RatParser::StmtContext::declarations() {
  return getRuleContext<RatParser::DeclarationsContext>(0);
}

RatParser::AssignmentStmtContext* RatParser::StmtContext::assignmentStmt() {
  return getRuleContext<RatParser::AssignmentStmtContext>(0);
}

RatParser::EchoStmtContext* RatParser::StmtContext::echoStmt() {
  return getRuleContext<RatParser::EchoStmtContext>(0);
}

RatParser::SposeStmtContext* RatParser::StmtContext::sposeStmt() {
  return getRuleContext<RatParser::SposeStmtContext>(0);
}

RatParser::ProcStmtContext* RatParser::StmtContext::procStmt() {
  return getRuleContext<RatParser::ProcStmtContext>(0);
}

RatParser::PrintStmtContext* RatParser::StmtContext::printStmt() {
  return getRuleContext<RatParser::PrintStmtContext>(0);
}


size_t RatParser::StmtContext::getRuleIndex() const {
  return RatParser::RuleStmt;
}


antlrcpp::Any RatParser::StmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::StmtContext* RatParser::stmt() {
  StmtContext *_localctx = _tracker.createInstance<StmtContext>(_ctx, getState());
  enterRule(_localctx, 28, RatParser::RuleStmt);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(182);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(175);
      declarations();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(176);
      assignmentStmt();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(177);
      echoStmt();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(178);
      sposeStmt();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(179);
      procStmt();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(180);
      printStmt();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);

      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StmtListContext ------------------------------------------------------------------

RatParser::StmtListContext::StmtListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::StmtContext *> RatParser::StmtListContext::stmt() {
  return getRuleContexts<RatParser::StmtContext>();
}

RatParser::StmtContext* RatParser::StmtListContext::stmt(size_t i) {
  return getRuleContext<RatParser::StmtContext>(i);
}


size_t RatParser::StmtListContext::getRuleIndex() const {
  return RatParser::RuleStmtList;
}


antlrcpp::Any RatParser::StmtListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitStmtList(this);
  else
    return visitor->visitChildren(this);
}

RatParser::StmtListContext* RatParser::stmtList() {
  StmtListContext *_localctx = _tracker.createInstance<StmtListContext>(_ctx, getState());
  enterRule(_localctx, 30, RatParser::RuleStmtList);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(184);
    stmt();
    setState(189);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(185);
        match(RatParser::T__2);
        setState(186);
        stmt(); 
      }
      setState(191);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentStmtContext ------------------------------------------------------------------

RatParser::AssignmentStmtContext::AssignmentStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::VariableContext* RatParser::AssignmentStmtContext::variable() {
  return getRuleContext<RatParser::VariableContext>(0);
}

tree::TerminalNode* RatParser::AssignmentStmtContext::ASSIGN() {
  return getToken(RatParser::ASSIGN, 0);
}

RatParser::ExprContext* RatParser::AssignmentStmtContext::expr() {
  return getRuleContext<RatParser::ExprContext>(0);
}


size_t RatParser::AssignmentStmtContext::getRuleIndex() const {
  return RatParser::RuleAssignmentStmt;
}


antlrcpp::Any RatParser::AssignmentStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitAssignmentStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::AssignmentStmtContext* RatParser::assignmentStmt() {
  AssignmentStmtContext *_localctx = _tracker.createInstance<AssignmentStmtContext>(_ctx, getState());
  enterRule(_localctx, 32, RatParser::RuleAssignmentStmt);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(192);
    variable();
    setState(193);
    match(RatParser::ASSIGN);
    setState(194);
    expr(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EchoStmtContext ------------------------------------------------------------------

RatParser::EchoStmtContext::EchoStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::EchoStmtContext::ECHO() {
  return getToken(RatParser::ECHO, 0);
}

RatParser::StmtListContext* RatParser::EchoStmtContext::stmtList() {
  return getRuleContext<RatParser::StmtListContext>(0);
}

tree::TerminalNode* RatParser::EchoStmtContext::TIL() {
  return getToken(RatParser::TIL, 0);
}

RatParser::RelOpExprContext* RatParser::EchoStmtContext::relOpExpr() {
  return getRuleContext<RatParser::RelOpExprContext>(0);
}


size_t RatParser::EchoStmtContext::getRuleIndex() const {
  return RatParser::RuleEchoStmt;
}


antlrcpp::Any RatParser::EchoStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitEchoStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::EchoStmtContext* RatParser::echoStmt() {
  EchoStmtContext *_localctx = _tracker.createInstance<EchoStmtContext>(_ctx, getState());
  enterRule(_localctx, 34, RatParser::RuleEchoStmt);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(196);
    match(RatParser::ECHO);
    setState(197);
    stmtList();
    setState(198);
    match(RatParser::TIL);
    setState(199);
    relOpExpr();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SposeStmtContext ------------------------------------------------------------------

RatParser::SposeStmtContext::SposeStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::SposeStmtContext::SPOSE() {
  return getToken(RatParser::SPOSE, 0);
}

RatParser::RelOpExprContext* RatParser::SposeStmtContext::relOpExpr() {
  return getRuleContext<RatParser::RelOpExprContext>(0);
}

tree::TerminalNode* RatParser::SposeStmtContext::USE() {
  return getToken(RatParser::USE, 0);
}

std::vector<RatParser::StmtListContext *> RatParser::SposeStmtContext::stmtList() {
  return getRuleContexts<RatParser::StmtListContext>();
}

RatParser::StmtListContext* RatParser::SposeStmtContext::stmtList(size_t i) {
  return getRuleContext<RatParser::StmtListContext>(i);
}

tree::TerminalNode* RatParser::SposeStmtContext::OWISE() {
  return getToken(RatParser::OWISE, 0);
}


size_t RatParser::SposeStmtContext::getRuleIndex() const {
  return RatParser::RuleSposeStmt;
}


antlrcpp::Any RatParser::SposeStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitSposeStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::SposeStmtContext* RatParser::sposeStmt() {
  SposeStmtContext *_localctx = _tracker.createInstance<SposeStmtContext>(_ctx, getState());
  enterRule(_localctx, 36, RatParser::RuleSposeStmt);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(201);
    match(RatParser::SPOSE);
    setState(202);
    relOpExpr();
    setState(203);
    match(RatParser::USE);
    setState(204);
    stmtList();
    setState(207);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx)) {
    case 1: {
      setState(205);
      match(RatParser::OWISE);
      setState(206);
      stmtList();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcStmtContext ------------------------------------------------------------------

RatParser::ProcStmtContext::ProcStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::MethodCallIdContext* RatParser::ProcStmtContext::methodCallId() {
  return getRuleContext<RatParser::MethodCallIdContext>(0);
}

RatParser::ArgumentListContext* RatParser::ProcStmtContext::argumentList() {
  return getRuleContext<RatParser::ArgumentListContext>(0);
}


size_t RatParser::ProcStmtContext::getRuleIndex() const {
  return RatParser::RuleProcStmt;
}


antlrcpp::Any RatParser::ProcStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitProcStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ProcStmtContext* RatParser::procStmt() {
  ProcStmtContext *_localctx = _tracker.createInstance<ProcStmtContext>(_ctx, getState());
  enterRule(_localctx, 38, RatParser::RuleProcStmt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(209);
    methodCallId();
    setState(210);
    match(RatParser::T__5);
    setState(212);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << RatParser::T__5)
      | (1ULL << RatParser::IDENTIFIER)
      | (1ULL << RatParser::INTEGER)
      | (1ULL << RatParser::FLOAT)
      | (1ULL << RatParser::STRING)
      | (1ULL << RatParser::ADD_OP)
      | (1ULL << RatParser::SUB_OP))) != 0)) {
      setState(211);
      argumentList();
    }
    setState(214);
    match(RatParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncStmtContext ------------------------------------------------------------------

RatParser::FuncStmtContext::FuncStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::MethodCallIdContext* RatParser::FuncStmtContext::methodCallId() {
  return getRuleContext<RatParser::MethodCallIdContext>(0);
}

RatParser::ArgumentListContext* RatParser::FuncStmtContext::argumentList() {
  return getRuleContext<RatParser::ArgumentListContext>(0);
}


size_t RatParser::FuncStmtContext::getRuleIndex() const {
  return RatParser::RuleFuncStmt;
}


antlrcpp::Any RatParser::FuncStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitFuncStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::FuncStmtContext* RatParser::funcStmt() {
  FuncStmtContext *_localctx = _tracker.createInstance<FuncStmtContext>(_ctx, getState());
  enterRule(_localctx, 40, RatParser::RuleFuncStmt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(216);
    methodCallId();
    setState(217);
    match(RatParser::T__5);
    setState(219);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << RatParser::T__5)
      | (1ULL << RatParser::IDENTIFIER)
      | (1ULL << RatParser::INTEGER)
      | (1ULL << RatParser::FLOAT)
      | (1ULL << RatParser::STRING)
      | (1ULL << RatParser::ADD_OP)
      | (1ULL << RatParser::SUB_OP))) != 0)) {
      setState(218);
      argumentList();
    }
    setState(221);
    match(RatParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PrintStmtContext ------------------------------------------------------------------

RatParser::PrintStmtContext::PrintStmtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::PrintStmtContext::PRINT() {
  return getToken(RatParser::PRINT, 0);
}

RatParser::StringContext* RatParser::PrintStmtContext::string() {
  return getRuleContext<RatParser::StringContext>(0);
}

RatParser::ValueContext* RatParser::PrintStmtContext::value() {
  return getRuleContext<RatParser::ValueContext>(0);
}


size_t RatParser::PrintStmtContext::getRuleIndex() const {
  return RatParser::RulePrintStmt;
}


antlrcpp::Any RatParser::PrintStmtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitPrintStmt(this);
  else
    return visitor->visitChildren(this);
}

RatParser::PrintStmtContext* RatParser::printStmt() {
  PrintStmtContext *_localctx = _tracker.createInstance<PrintStmtContext>(_ctx, getState());
  enterRule(_localctx, 42, RatParser::RulePrintStmt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    match(RatParser::PRINT);
    setState(224);
    match(RatParser::T__5);
    setState(225);
    string();
    setState(228);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == RatParser::T__4) {
      setState(226);
      match(RatParser::T__4);
      setState(227);
      value();
    }
    setState(230);
    match(RatParser::T__6);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MethodCallIdContext ------------------------------------------------------------------

RatParser::MethodCallIdContext::MethodCallIdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::MethodCallIdContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::MethodCallIdContext::getRuleIndex() const {
  return RatParser::RuleMethodCallId;
}


antlrcpp::Any RatParser::MethodCallIdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitMethodCallId(this);
  else
    return visitor->visitChildren(this);
}

RatParser::MethodCallIdContext* RatParser::methodCallId() {
  MethodCallIdContext *_localctx = _tracker.createInstance<MethodCallIdContext>(_ctx, getState());
  enterRule(_localctx, 44, RatParser::RuleMethodCallId);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(232);
    match(RatParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValueContext ------------------------------------------------------------------

RatParser::ValueContext::ValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::ExprContext *> RatParser::ValueContext::expr() {
  return getRuleContexts<RatParser::ExprContext>();
}

RatParser::ExprContext* RatParser::ValueContext::expr(size_t i) {
  return getRuleContext<RatParser::ExprContext>(i);
}


size_t RatParser::ValueContext::getRuleIndex() const {
  return RatParser::RuleValue;
}


antlrcpp::Any RatParser::ValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitValue(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ValueContext* RatParser::value() {
  ValueContext *_localctx = _tracker.createInstance<ValueContext>(_ctx, getState());
  enterRule(_localctx, 46, RatParser::RuleValue);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(234);
    expr(0);
    setState(239);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::T__4) {
      setState(235);
      match(RatParser::T__4);
      setState(236);
      expr(0);
      setState(241);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableContext ------------------------------------------------------------------

RatParser::VariableContext::VariableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::VariableContext::IDENTIFIER() {
  return getToken(RatParser::IDENTIFIER, 0);
}


size_t RatParser::VariableContext::getRuleIndex() const {
  return RatParser::RuleVariable;
}


antlrcpp::Any RatParser::VariableContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitVariable(this);
  else
    return visitor->visitChildren(this);
}

RatParser::VariableContext* RatParser::variable() {
  VariableContext *_localctx = _tracker.createInstance<VariableContext>(_ctx, getState());
  enterRule(_localctx, 48, RatParser::RuleVariable);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(242);
    match(RatParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgumentListContext ------------------------------------------------------------------

RatParser::ArgumentListContext::ArgumentListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::ExprContext *> RatParser::ArgumentListContext::expr() {
  return getRuleContexts<RatParser::ExprContext>();
}

RatParser::ExprContext* RatParser::ArgumentListContext::expr(size_t i) {
  return getRuleContext<RatParser::ExprContext>(i);
}


size_t RatParser::ArgumentListContext::getRuleIndex() const {
  return RatParser::RuleArgumentList;
}


antlrcpp::Any RatParser::ArgumentListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitArgumentList(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ArgumentListContext* RatParser::argumentList() {
  ArgumentListContext *_localctx = _tracker.createInstance<ArgumentListContext>(_ctx, getState());
  enterRule(_localctx, 50, RatParser::RuleArgumentList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(244);
    expr(0);
    setState(249);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == RatParser::T__4) {
      setState(245);
      match(RatParser::T__4);
      setState(246);
      expr(0);
      setState(251);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

RatParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t RatParser::ExprContext::getRuleIndex() const {
  return RatParser::RuleExpr;
}

void RatParser::ExprContext::copyFrom(ExprContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
  this->type = ctx->type;
}

//----------------- StringExprContext ------------------------------------------------------------------

RatParser::StringContext* RatParser::StringExprContext::string() {
  return getRuleContext<RatParser::StringContext>(0);
}

RatParser::StringExprContext::StringExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::StringExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitStringExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- VariableExprContext ------------------------------------------------------------------

RatParser::VariableContext* RatParser::VariableExprContext::variable() {
  return getRuleContext<RatParser::VariableContext>(0);
}

RatParser::VariableExprContext::VariableExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::VariableExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitVariableExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AddSubExprContext ------------------------------------------------------------------

std::vector<RatParser::ExprContext *> RatParser::AddSubExprContext::expr() {
  return getRuleContexts<RatParser::ExprContext>();
}

RatParser::ExprContext* RatParser::AddSubExprContext::expr(size_t i) {
  return getRuleContext<RatParser::ExprContext>(i);
}

RatParser::AddSubOpContext* RatParser::AddSubExprContext::addSubOp() {
  return getRuleContext<RatParser::AddSubOpContext>(0);
}

RatParser::AddSubExprContext::AddSubExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::AddSubExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitAddSubExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FuncStmtExprContext ------------------------------------------------------------------

RatParser::FuncStmtContext* RatParser::FuncStmtExprContext::funcStmt() {
  return getRuleContext<RatParser::FuncStmtContext>(0);
}

RatParser::FuncStmtExprContext::FuncStmtExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::FuncStmtExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitFuncStmtExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- UnsignedNumberExprContext ------------------------------------------------------------------

RatParser::NumberContext* RatParser::UnsignedNumberExprContext::number() {
  return getRuleContext<RatParser::NumberContext>(0);
}

RatParser::UnsignedNumberExprContext::UnsignedNumberExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::UnsignedNumberExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitUnsignedNumberExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- MulDivExprContext ------------------------------------------------------------------

std::vector<RatParser::ExprContext *> RatParser::MulDivExprContext::expr() {
  return getRuleContexts<RatParser::ExprContext>();
}

RatParser::ExprContext* RatParser::MulDivExprContext::expr(size_t i) {
  return getRuleContext<RatParser::ExprContext>(i);
}

RatParser::MulDivOpContext* RatParser::MulDivExprContext::mulDivOp() {
  return getRuleContext<RatParser::MulDivOpContext>(0);
}

RatParser::MulDivExprContext::MulDivExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::MulDivExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitMulDivExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParenExprContext ------------------------------------------------------------------

RatParser::ExprContext* RatParser::ParenExprContext::expr() {
  return getRuleContext<RatParser::ExprContext>(0);
}

RatParser::ParenExprContext::ParenExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::ParenExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitParenExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SignedNumberExprContext ------------------------------------------------------------------

RatParser::SignedNumberContext* RatParser::SignedNumberExprContext::signedNumber() {
  return getRuleContext<RatParser::SignedNumberContext>(0);
}

RatParser::SignedNumberExprContext::SignedNumberExprContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::SignedNumberExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitSignedNumberExpr(this);
  else
    return visitor->visitChildren(this);
}

RatParser::ExprContext* RatParser::expr() {
   return expr(0);
}

RatParser::ExprContext* RatParser::expr(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  RatParser::ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, parentState);
  RatParser::ExprContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 52;
  enterRecursionRule(_localctx, 52, RatParser::RuleExpr, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(262);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<UnsignedNumberExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(253);
      number();
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<SignedNumberExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(254);
      signedNumber();
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<VariableExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(255);
      variable();
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<StringExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(256);
      string();
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<ParenExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(257);
      match(RatParser::T__5);
      setState(258);
      expr(0);
      setState(259);
      match(RatParser::T__6);
      break;
    }

    case 6: {
      _localctx = _tracker.createInstance<FuncStmtExprContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(261);
      funcStmt();
      break;
    }

    }
    _ctx->stop = _input->LT(-1);
    setState(274);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(272);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<MulDivExprContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(264);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(265);
          mulDivOp();
          setState(266);
          expr(9);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<AddSubExprContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(268);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(269);
          addSubOp();
          setState(270);
          expr(8);
          break;
        }

        } 
      }
      setState(276);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- RelOpExprContext ------------------------------------------------------------------

RatParser::RelOpExprContext::RelOpExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<RatParser::ExprContext *> RatParser::RelOpExprContext::expr() {
  return getRuleContexts<RatParser::ExprContext>();
}

RatParser::ExprContext* RatParser::RelOpExprContext::expr(size_t i) {
  return getRuleContext<RatParser::ExprContext>(i);
}

RatParser::RelOpContext* RatParser::RelOpExprContext::relOp() {
  return getRuleContext<RatParser::RelOpContext>(0);
}


size_t RatParser::RelOpExprContext::getRuleIndex() const {
  return RatParser::RuleRelOpExpr;
}


antlrcpp::Any RatParser::RelOpExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitRelOpExpr(this);
  else
    return visitor->visitChildren(this);
}

RatParser::RelOpExprContext* RatParser::relOpExpr() {
  RelOpExprContext *_localctx = _tracker.createInstance<RelOpExprContext>(_ctx, getState());
  enterRule(_localctx, 54, RatParser::RuleRelOpExpr);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(277);
    expr(0);
    setState(278);
    relOp();
    setState(279);
    expr(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SignedNumberContext ------------------------------------------------------------------

RatParser::SignedNumberContext::SignedNumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

RatParser::SignContext* RatParser::SignedNumberContext::sign() {
  return getRuleContext<RatParser::SignContext>(0);
}

RatParser::NumberContext* RatParser::SignedNumberContext::number() {
  return getRuleContext<RatParser::NumberContext>(0);
}


size_t RatParser::SignedNumberContext::getRuleIndex() const {
  return RatParser::RuleSignedNumber;
}


antlrcpp::Any RatParser::SignedNumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitSignedNumber(this);
  else
    return visitor->visitChildren(this);
}

RatParser::SignedNumberContext* RatParser::signedNumber() {
  SignedNumberContext *_localctx = _tracker.createInstance<SignedNumberContext>(_ctx, getState());
  enterRule(_localctx, 56, RatParser::RuleSignedNumber);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(281);
    sign();
    setState(282);
    number();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SignContext ------------------------------------------------------------------

RatParser::SignContext::SignContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::SignContext::ADD_OP() {
  return getToken(RatParser::ADD_OP, 0);
}

tree::TerminalNode* RatParser::SignContext::SUB_OP() {
  return getToken(RatParser::SUB_OP, 0);
}


size_t RatParser::SignContext::getRuleIndex() const {
  return RatParser::RuleSign;
}


antlrcpp::Any RatParser::SignContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitSign(this);
  else
    return visitor->visitChildren(this);
}

RatParser::SignContext* RatParser::sign() {
  SignContext *_localctx = _tracker.createInstance<SignContext>(_ctx, getState());
  enterRule(_localctx, 58, RatParser::RuleSign);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(284);
    _la = _input->LA(1);
    if (!(_la == RatParser::ADD_OP

    || _la == RatParser::SUB_OP)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

RatParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t RatParser::NumberContext::getRuleIndex() const {
  return RatParser::RuleNumber;
}

void RatParser::NumberContext::copyFrom(NumberContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
  this->type = ctx->type;
}

//----------------- FloatConstContext ------------------------------------------------------------------

tree::TerminalNode* RatParser::FloatConstContext::FLOAT() {
  return getToken(RatParser::FLOAT, 0);
}

RatParser::FloatConstContext::FloatConstContext(NumberContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::FloatConstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitFloatConst(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IntegerConstContext ------------------------------------------------------------------

tree::TerminalNode* RatParser::IntegerConstContext::INTEGER() {
  return getToken(RatParser::INTEGER, 0);
}

RatParser::IntegerConstContext::IntegerConstContext(NumberContext *ctx) { copyFrom(ctx); }


antlrcpp::Any RatParser::IntegerConstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitIntegerConst(this);
  else
    return visitor->visitChildren(this);
}
RatParser::NumberContext* RatParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 60, RatParser::RuleNumber);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(288);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case RatParser::INTEGER: {
        _localctx = dynamic_cast<NumberContext *>(_tracker.createInstance<RatParser::IntegerConstContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(286);
        match(RatParser::INTEGER);
        break;
      }

      case RatParser::FLOAT: {
        _localctx = dynamic_cast<NumberContext *>(_tracker.createInstance<RatParser::FloatConstContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(287);
        match(RatParser::FLOAT);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringContext ------------------------------------------------------------------

RatParser::StringContext::StringContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::StringContext::STRING() {
  return getToken(RatParser::STRING, 0);
}


size_t RatParser::StringContext::getRuleIndex() const {
  return RatParser::RuleString;
}


antlrcpp::Any RatParser::StringContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitString(this);
  else
    return visitor->visitChildren(this);
}

RatParser::StringContext* RatParser::string() {
  StringContext *_localctx = _tracker.createInstance<StringContext>(_ctx, getState());
  enterRule(_localctx, 62, RatParser::RuleString);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(290);
    match(RatParser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MulDivOpContext ------------------------------------------------------------------

RatParser::MulDivOpContext::MulDivOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::MulDivOpContext::MUL_OP() {
  return getToken(RatParser::MUL_OP, 0);
}

tree::TerminalNode* RatParser::MulDivOpContext::DIV_OP() {
  return getToken(RatParser::DIV_OP, 0);
}


size_t RatParser::MulDivOpContext::getRuleIndex() const {
  return RatParser::RuleMulDivOp;
}


antlrcpp::Any RatParser::MulDivOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitMulDivOp(this);
  else
    return visitor->visitChildren(this);
}

RatParser::MulDivOpContext* RatParser::mulDivOp() {
  MulDivOpContext *_localctx = _tracker.createInstance<MulDivOpContext>(_ctx, getState());
  enterRule(_localctx, 64, RatParser::RuleMulDivOp);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(292);
    _la = _input->LA(1);
    if (!(_la == RatParser::MUL_OP

    || _la == RatParser::DIV_OP)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddSubOpContext ------------------------------------------------------------------

RatParser::AddSubOpContext::AddSubOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::AddSubOpContext::ADD_OP() {
  return getToken(RatParser::ADD_OP, 0);
}

tree::TerminalNode* RatParser::AddSubOpContext::SUB_OP() {
  return getToken(RatParser::SUB_OP, 0);
}


size_t RatParser::AddSubOpContext::getRuleIndex() const {
  return RatParser::RuleAddSubOp;
}


antlrcpp::Any RatParser::AddSubOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitAddSubOp(this);
  else
    return visitor->visitChildren(this);
}

RatParser::AddSubOpContext* RatParser::addSubOp() {
  AddSubOpContext *_localctx = _tracker.createInstance<AddSubOpContext>(_ctx, getState());
  enterRule(_localctx, 66, RatParser::RuleAddSubOp);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(294);
    _la = _input->LA(1);
    if (!(_la == RatParser::ADD_OP

    || _la == RatParser::SUB_OP)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RelOpContext ------------------------------------------------------------------

RatParser::RelOpContext::RelOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* RatParser::RelOpContext::EQ_OP() {
  return getToken(RatParser::EQ_OP, 0);
}

tree::TerminalNode* RatParser::RelOpContext::NE_OP() {
  return getToken(RatParser::NE_OP, 0);
}

tree::TerminalNode* RatParser::RelOpContext::LT_OP() {
  return getToken(RatParser::LT_OP, 0);
}

tree::TerminalNode* RatParser::RelOpContext::LE_OP() {
  return getToken(RatParser::LE_OP, 0);
}

tree::TerminalNode* RatParser::RelOpContext::GT_OP() {
  return getToken(RatParser::GT_OP, 0);
}

tree::TerminalNode* RatParser::RelOpContext::GE_OP() {
  return getToken(RatParser::GE_OP, 0);
}


size_t RatParser::RelOpContext::getRuleIndex() const {
  return RatParser::RuleRelOp;
}


antlrcpp::Any RatParser::RelOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<RatVisitor*>(visitor))
    return parserVisitor->visitRelOp(this);
  else
    return visitor->visitChildren(this);
}

RatParser::RelOpContext* RatParser::relOp() {
  RelOpContext *_localctx = _tracker.createInstance<RelOpContext>(_ctx, getState());
  enterRule(_localctx, 68, RatParser::RuleRelOp);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(296);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << RatParser::EQ_OP)
      | (1ULL << RatParser::NE_OP)
      | (1ULL << RatParser::LT_OP)
      | (1ULL << RatParser::LE_OP)
      | (1ULL << RatParser::GT_OP)
      | (1ULL << RatParser::GE_OP))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool RatParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 26: return exprSempred(dynamic_cast<ExprContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool RatParser::exprSempred(ExprContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 8);
    case 1: return precpred(_ctx, 7);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> RatParser::_decisionToDFA;
atn::PredictionContextCache RatParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN RatParser::_atn;
std::vector<uint16_t> RatParser::_serializedATN;

std::vector<std::string> RatParser::_ruleNames = {
  "program", "header", "mainBlock", "block", "declarations", "decList", 
  "decl", "varList", "varId", "typeId", "procDecl", "funcDecl", "methodId", 
  "paramList", "stmt", "stmtList", "assignmentStmt", "echoStmt", "sposeStmt", 
  "procStmt", "funcStmt", "printStmt", "methodCallId", "value", "variable", 
  "argumentList", "expr", "relOpExpr", "signedNumber", "sign", "number", 
  "string", "mulDivOp", "addSubOp", "relOp"
};

std::vector<std::string> RatParser::_literalNames = {
  "", "'?'", "'_'", "'.'", "':'", "','", "'('", "')'", "';'", "'TRIGGER'", 
  "'PULLOUT'", "'IDS'", "'ECHO'", "'TIL'", "'SPOSE'", "'USE'", "'OWISE'", 
  "'PROC'", "'FUNC'", "'PRINT'", "'RETURN'", "", "", "", "", "'*'", "'/'", 
  "'+'", "'-'", "'='", "'=='", "'<>'", "'<'", "'<='", "'>'", "'>='"
};

std::vector<std::string> RatParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "TRIGGER", "PULLOUT", "IDS", "ECHO", 
  "TIL", "SPOSE", "USE", "OWISE", "PROC", "FUNC", "PRINT", "RETURN", "IDENTIFIER", 
  "INTEGER", "FLOAT", "STRING", "MUL_OP", "DIV_OP", "ADD_OP", "SUB_OP", 
  "ASSIGN", "EQ_OP", "NE_OP", "LT_OP", "LE_OP", "GT_OP", "GE_OP", "NEWLINE", 
  "WS"
};

dfa::Vocabulary RatParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> RatParser::_tokenNames;

RatParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x27, 0x12d, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x3, 0x2, 0x3, 
    0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x5, 0x5, 0x5, 0x54, 0xa, 0x5, 0x3, 0x5, 0x7, 0x5, 
    0x57, 0xa, 0x5, 0xc, 0x5, 0xe, 0x5, 0x5a, 0xb, 0x5, 0x3, 0x5, 0x7, 0x5, 
    0x5d, 0xa, 0x5, 0xc, 0x5, 0xe, 0x5, 0x60, 0xb, 0x5, 0x3, 0x5, 0x3, 0x5, 
    0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 
    0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x6d, 0xa, 0x7, 0xc, 0x7, 0xe, 0x7, 0x70, 
    0xb, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x9, 0x3, 0x9, 
    0x3, 0x9, 0x7, 0x9, 0x79, 0xa, 0x9, 0xc, 0x9, 0xe, 0x9, 0x7c, 0xb, 0x9, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 
    0x3, 0xc, 0x5, 0xc, 0x86, 0xa, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 
    0xc, 0x5, 0xc, 0x8c, 0xa, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0x95, 0xa, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0x9d, 0xa, 0xd, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0xa3, 0xa, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x7, 0xf, 0xad, 0xa, 0xf, 0xc, 0xf, 0xe, 0xf, 0xb0, 0xb, 0xf, 0x3, 
    0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x5, 0x10, 0xb9, 0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x7, 0x11, 
    0xbe, 0xa, 0x11, 0xc, 0x11, 0xe, 0x11, 0xc1, 0xb, 0x11, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 
    0x14, 0x5, 0x14, 0xd2, 0xa, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 
    0x15, 0xd7, 0xa, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 
    0x16, 0x5, 0x16, 0xde, 0xa, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 0x3, 
    0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x5, 0x17, 0xe7, 0xa, 0x17, 0x3, 
    0x17, 0x3, 0x17, 0x3, 0x18, 0x3, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 
    0x7, 0x19, 0xf0, 0xa, 0x19, 0xc, 0x19, 0xe, 0x19, 0xf3, 0xb, 0x19, 0x3, 
    0x1a, 0x3, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x7, 0x1b, 0xfa, 0xa, 
    0x1b, 0xc, 0x1b, 0xe, 0x1b, 0xfd, 0xb, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 
    0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 
    0x3, 0x1c, 0x5, 0x1c, 0x109, 0xa, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 
    0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x7, 0x1c, 0x113, 
    0xa, 0x1c, 0xc, 0x1c, 0xe, 0x1c, 0x116, 0xb, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 
    0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1f, 0x3, 
    0x1f, 0x3, 0x20, 0x3, 0x20, 0x5, 0x20, 0x123, 0xa, 0x20, 0x3, 0x21, 
    0x3, 0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 0x23, 0x3, 0x23, 0x3, 0x24, 0x3, 
    0x24, 0x3, 0x24, 0x2, 0x3, 0x36, 0x25, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 
    0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 
    0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 
    0x3e, 0x40, 0x42, 0x44, 0x46, 0x2, 0x5, 0x3, 0x2, 0x1d, 0x1e, 0x3, 0x2, 
    0x1b, 0x1c, 0x3, 0x2, 0x20, 0x25, 0x2, 0x129, 0x2, 0x48, 0x3, 0x2, 0x2, 
    0x2, 0x4, 0x4c, 0x3, 0x2, 0x2, 0x2, 0x6, 0x50, 0x3, 0x2, 0x2, 0x2, 0x8, 
    0x53, 0x3, 0x2, 0x2, 0x2, 0xa, 0x65, 0x3, 0x2, 0x2, 0x2, 0xc, 0x69, 
    0x3, 0x2, 0x2, 0x2, 0xe, 0x71, 0x3, 0x2, 0x2, 0x2, 0x10, 0x75, 0x3, 
    0x2, 0x2, 0x2, 0x12, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x14, 0x7f, 0x3, 0x2, 
    0x2, 0x2, 0x16, 0x81, 0x3, 0x2, 0x2, 0x2, 0x18, 0x90, 0x3, 0x2, 0x2, 
    0x2, 0x1a, 0xa7, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xa9, 0x3, 0x2, 0x2, 0x2, 
    0x1e, 0xb8, 0x3, 0x2, 0x2, 0x2, 0x20, 0xba, 0x3, 0x2, 0x2, 0x2, 0x22, 
    0xc2, 0x3, 0x2, 0x2, 0x2, 0x24, 0xc6, 0x3, 0x2, 0x2, 0x2, 0x26, 0xcb, 
    0x3, 0x2, 0x2, 0x2, 0x28, 0xd3, 0x3, 0x2, 0x2, 0x2, 0x2a, 0xda, 0x3, 
    0x2, 0x2, 0x2, 0x2c, 0xe1, 0x3, 0x2, 0x2, 0x2, 0x2e, 0xea, 0x3, 0x2, 
    0x2, 0x2, 0x30, 0xec, 0x3, 0x2, 0x2, 0x2, 0x32, 0xf4, 0x3, 0x2, 0x2, 
    0x2, 0x34, 0xf6, 0x3, 0x2, 0x2, 0x2, 0x36, 0x108, 0x3, 0x2, 0x2, 0x2, 
    0x38, 0x117, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x11b, 0x3, 0x2, 0x2, 0x2, 0x3c, 
    0x11e, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x122, 0x3, 0x2, 0x2, 0x2, 0x40, 0x124, 
    0x3, 0x2, 0x2, 0x2, 0x42, 0x126, 0x3, 0x2, 0x2, 0x2, 0x44, 0x128, 0x3, 
    0x2, 0x2, 0x2, 0x46, 0x12a, 0x3, 0x2, 0x2, 0x2, 0x48, 0x49, 0x5, 0x4, 
    0x3, 0x2, 0x49, 0x4a, 0x5, 0x6, 0x4, 0x2, 0x4a, 0x4b, 0x7, 0x3, 0x2, 
    0x2, 0x4b, 0x3, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x4d, 0x7, 0x4, 0x2, 0x2, 
    0x4d, 0x4e, 0x7, 0x17, 0x2, 0x2, 0x4e, 0x4f, 0x7, 0x5, 0x2, 0x2, 0x4f, 
    0x5, 0x3, 0x2, 0x2, 0x2, 0x50, 0x51, 0x5, 0x8, 0x5, 0x2, 0x51, 0x7, 
    0x3, 0x2, 0x2, 0x2, 0x52, 0x54, 0x5, 0xa, 0x6, 0x2, 0x53, 0x52, 0x3, 
    0x2, 0x2, 0x2, 0x53, 0x54, 0x3, 0x2, 0x2, 0x2, 0x54, 0x58, 0x3, 0x2, 
    0x2, 0x2, 0x55, 0x57, 0x5, 0x16, 0xc, 0x2, 0x56, 0x55, 0x3, 0x2, 0x2, 
    0x2, 0x57, 0x5a, 0x3, 0x2, 0x2, 0x2, 0x58, 0x56, 0x3, 0x2, 0x2, 0x2, 
    0x58, 0x59, 0x3, 0x2, 0x2, 0x2, 0x59, 0x5e, 0x3, 0x2, 0x2, 0x2, 0x5a, 
    0x58, 0x3, 0x2, 0x2, 0x2, 0x5b, 0x5d, 0x5, 0x18, 0xd, 0x2, 0x5c, 0x5b, 
    0x3, 0x2, 0x2, 0x2, 0x5d, 0x60, 0x3, 0x2, 0x2, 0x2, 0x5e, 0x5c, 0x3, 
    0x2, 0x2, 0x2, 0x5e, 0x5f, 0x3, 0x2, 0x2, 0x2, 0x5f, 0x61, 0x3, 0x2, 
    0x2, 0x2, 0x60, 0x5e, 0x3, 0x2, 0x2, 0x2, 0x61, 0x62, 0x7, 0xb, 0x2, 
    0x2, 0x62, 0x63, 0x5, 0x20, 0x11, 0x2, 0x63, 0x64, 0x7, 0xc, 0x2, 0x2, 
    0x64, 0x9, 0x3, 0x2, 0x2, 0x2, 0x65, 0x66, 0x7, 0xd, 0x2, 0x2, 0x66, 
    0x67, 0x5, 0xc, 0x7, 0x2, 0x67, 0x68, 0x7, 0x5, 0x2, 0x2, 0x68, 0xb, 
    0x3, 0x2, 0x2, 0x2, 0x69, 0x6e, 0x5, 0xe, 0x8, 0x2, 0x6a, 0x6b, 0x7, 
    0x5, 0x2, 0x2, 0x6b, 0x6d, 0x5, 0xe, 0x8, 0x2, 0x6c, 0x6a, 0x3, 0x2, 
    0x2, 0x2, 0x6d, 0x70, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x6c, 0x3, 0x2, 0x2, 
    0x2, 0x6e, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x6f, 0xd, 0x3, 0x2, 0x2, 0x2, 
    0x70, 0x6e, 0x3, 0x2, 0x2, 0x2, 0x71, 0x72, 0x5, 0x10, 0x9, 0x2, 0x72, 
    0x73, 0x7, 0x6, 0x2, 0x2, 0x73, 0x74, 0x5, 0x14, 0xb, 0x2, 0x74, 0xf, 
    0x3, 0x2, 0x2, 0x2, 0x75, 0x7a, 0x5, 0x12, 0xa, 0x2, 0x76, 0x77, 0x7, 
    0x7, 0x2, 0x2, 0x77, 0x79, 0x5, 0x12, 0xa, 0x2, 0x78, 0x76, 0x3, 0x2, 
    0x2, 0x2, 0x79, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x7a, 0x78, 0x3, 0x2, 0x2, 
    0x2, 0x7a, 0x7b, 0x3, 0x2, 0x2, 0x2, 0x7b, 0x11, 0x3, 0x2, 0x2, 0x2, 
    0x7c, 0x7a, 0x3, 0x2, 0x2, 0x2, 0x7d, 0x7e, 0x7, 0x17, 0x2, 0x2, 0x7e, 
    0x13, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x80, 0x7, 0x17, 0x2, 0x2, 0x80, 0x15, 
    0x3, 0x2, 0x2, 0x2, 0x81, 0x82, 0x7, 0x13, 0x2, 0x2, 0x82, 0x83, 0x5, 
    0x1a, 0xe, 0x2, 0x83, 0x85, 0x7, 0x8, 0x2, 0x2, 0x84, 0x86, 0x5, 0x1c, 
    0xf, 0x2, 0x85, 0x84, 0x3, 0x2, 0x2, 0x2, 0x85, 0x86, 0x3, 0x2, 0x2, 
    0x2, 0x86, 0x87, 0x3, 0x2, 0x2, 0x2, 0x87, 0x88, 0x7, 0x9, 0x2, 0x2, 
    0x88, 0x89, 0x7, 0x5, 0x2, 0x2, 0x89, 0x8b, 0x7, 0xb, 0x2, 0x2, 0x8a, 
    0x8c, 0x5, 0x20, 0x11, 0x2, 0x8b, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x8c, 
    0x3, 0x2, 0x2, 0x2, 0x8c, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x8d, 0x8e, 0x7, 
    0xc, 0x2, 0x2, 0x8e, 0x8f, 0x7, 0x5, 0x2, 0x2, 0x8f, 0x17, 0x3, 0x2, 
    0x2, 0x2, 0x90, 0x91, 0x7, 0x14, 0x2, 0x2, 0x91, 0x92, 0x5, 0x1a, 0xe, 
    0x2, 0x92, 0x94, 0x7, 0x8, 0x2, 0x2, 0x93, 0x95, 0x5, 0x1c, 0xf, 0x2, 
    0x94, 0x93, 0x3, 0x2, 0x2, 0x2, 0x94, 0x95, 0x3, 0x2, 0x2, 0x2, 0x95, 
    0x96, 0x3, 0x2, 0x2, 0x2, 0x96, 0x97, 0x7, 0x9, 0x2, 0x2, 0x97, 0x98, 
    0x7, 0x6, 0x2, 0x2, 0x98, 0x99, 0x5, 0x14, 0xb, 0x2, 0x99, 0x9a, 0x7, 
    0x5, 0x2, 0x2, 0x9a, 0x9c, 0x7, 0xb, 0x2, 0x2, 0x9b, 0x9d, 0x5, 0x20, 
    0x11, 0x2, 0x9c, 0x9b, 0x3, 0x2, 0x2, 0x2, 0x9c, 0x9d, 0x3, 0x2, 0x2, 
    0x2, 0x9d, 0xa2, 0x3, 0x2, 0x2, 0x2, 0x9e, 0x9f, 0x7, 0x16, 0x2, 0x2, 
    0x9f, 0xa0, 0x5, 0x36, 0x1c, 0x2, 0xa0, 0xa1, 0x7, 0x5, 0x2, 0x2, 0xa1, 
    0xa3, 0x3, 0x2, 0x2, 0x2, 0xa2, 0x9e, 0x3, 0x2, 0x2, 0x2, 0xa2, 0xa3, 
    0x3, 0x2, 0x2, 0x2, 0xa3, 0xa4, 0x3, 0x2, 0x2, 0x2, 0xa4, 0xa5, 0x7, 
    0xc, 0x2, 0x2, 0xa5, 0xa6, 0x7, 0x5, 0x2, 0x2, 0xa6, 0x19, 0x3, 0x2, 
    0x2, 0x2, 0xa7, 0xa8, 0x7, 0x17, 0x2, 0x2, 0xa8, 0x1b, 0x3, 0x2, 0x2, 
    0x2, 0xa9, 0xae, 0x5, 0xe, 0x8, 0x2, 0xaa, 0xab, 0x7, 0xa, 0x2, 0x2, 
    0xab, 0xad, 0x5, 0xe, 0x8, 0x2, 0xac, 0xaa, 0x3, 0x2, 0x2, 0x2, 0xad, 
    0xb0, 0x3, 0x2, 0x2, 0x2, 0xae, 0xac, 0x3, 0x2, 0x2, 0x2, 0xae, 0xaf, 
    0x3, 0x2, 0x2, 0x2, 0xaf, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xb0, 0xae, 0x3, 
    0x2, 0x2, 0x2, 0xb1, 0xb9, 0x5, 0xa, 0x6, 0x2, 0xb2, 0xb9, 0x5, 0x22, 
    0x12, 0x2, 0xb3, 0xb9, 0x5, 0x24, 0x13, 0x2, 0xb4, 0xb9, 0x5, 0x26, 
    0x14, 0x2, 0xb5, 0xb9, 0x5, 0x28, 0x15, 0x2, 0xb6, 0xb9, 0x5, 0x2c, 
    0x17, 0x2, 0xb7, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb1, 0x3, 0x2, 0x2, 
    0x2, 0xb8, 0xb2, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb3, 0x3, 0x2, 0x2, 0x2, 
    0xb8, 0xb4, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb5, 0x3, 0x2, 0x2, 0x2, 0xb8, 
    0xb6, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb7, 0x3, 0x2, 0x2, 0x2, 0xb9, 0x1f, 
    0x3, 0x2, 0x2, 0x2, 0xba, 0xbf, 0x5, 0x1e, 0x10, 0x2, 0xbb, 0xbc, 0x7, 
    0x5, 0x2, 0x2, 0xbc, 0xbe, 0x5, 0x1e, 0x10, 0x2, 0xbd, 0xbb, 0x3, 0x2, 
    0x2, 0x2, 0xbe, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xbd, 0x3, 0x2, 0x2, 
    0x2, 0xbf, 0xc0, 0x3, 0x2, 0x2, 0x2, 0xc0, 0x21, 0x3, 0x2, 0x2, 0x2, 
    0xc1, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc3, 0x5, 0x32, 0x1a, 0x2, 0xc3, 
    0xc4, 0x7, 0x1f, 0x2, 0x2, 0xc4, 0xc5, 0x5, 0x36, 0x1c, 0x2, 0xc5, 0x23, 
    0x3, 0x2, 0x2, 0x2, 0xc6, 0xc7, 0x7, 0xe, 0x2, 0x2, 0xc7, 0xc8, 0x5, 
    0x20, 0x11, 0x2, 0xc8, 0xc9, 0x7, 0xf, 0x2, 0x2, 0xc9, 0xca, 0x5, 0x38, 
    0x1d, 0x2, 0xca, 0x25, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xcc, 0x7, 0x10, 0x2, 
    0x2, 0xcc, 0xcd, 0x5, 0x38, 0x1d, 0x2, 0xcd, 0xce, 0x7, 0x11, 0x2, 0x2, 
    0xce, 0xd1, 0x5, 0x20, 0x11, 0x2, 0xcf, 0xd0, 0x7, 0x12, 0x2, 0x2, 0xd0, 
    0xd2, 0x5, 0x20, 0x11, 0x2, 0xd1, 0xcf, 0x3, 0x2, 0x2, 0x2, 0xd1, 0xd2, 
    0x3, 0x2, 0x2, 0x2, 0xd2, 0x27, 0x3, 0x2, 0x2, 0x2, 0xd3, 0xd4, 0x5, 
    0x2e, 0x18, 0x2, 0xd4, 0xd6, 0x7, 0x8, 0x2, 0x2, 0xd5, 0xd7, 0x5, 0x34, 
    0x1b, 0x2, 0xd6, 0xd5, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x3, 0x2, 0x2, 
    0x2, 0xd7, 0xd8, 0x3, 0x2, 0x2, 0x2, 0xd8, 0xd9, 0x7, 0x9, 0x2, 0x2, 
    0xd9, 0x29, 0x3, 0x2, 0x2, 0x2, 0xda, 0xdb, 0x5, 0x2e, 0x18, 0x2, 0xdb, 
    0xdd, 0x7, 0x8, 0x2, 0x2, 0xdc, 0xde, 0x5, 0x34, 0x1b, 0x2, 0xdd, 0xdc, 
    0x3, 0x2, 0x2, 0x2, 0xdd, 0xde, 0x3, 0x2, 0x2, 0x2, 0xde, 0xdf, 0x3, 
    0x2, 0x2, 0x2, 0xdf, 0xe0, 0x7, 0x9, 0x2, 0x2, 0xe0, 0x2b, 0x3, 0x2, 
    0x2, 0x2, 0xe1, 0xe2, 0x7, 0x15, 0x2, 0x2, 0xe2, 0xe3, 0x7, 0x8, 0x2, 
    0x2, 0xe3, 0xe6, 0x5, 0x40, 0x21, 0x2, 0xe4, 0xe5, 0x7, 0x7, 0x2, 0x2, 
    0xe5, 0xe7, 0x5, 0x30, 0x19, 0x2, 0xe6, 0xe4, 0x3, 0x2, 0x2, 0x2, 0xe6, 
    0xe7, 0x3, 0x2, 0x2, 0x2, 0xe7, 0xe8, 0x3, 0x2, 0x2, 0x2, 0xe8, 0xe9, 
    0x7, 0x9, 0x2, 0x2, 0xe9, 0x2d, 0x3, 0x2, 0x2, 0x2, 0xea, 0xeb, 0x7, 
    0x17, 0x2, 0x2, 0xeb, 0x2f, 0x3, 0x2, 0x2, 0x2, 0xec, 0xf1, 0x5, 0x36, 
    0x1c, 0x2, 0xed, 0xee, 0x7, 0x7, 0x2, 0x2, 0xee, 0xf0, 0x5, 0x36, 0x1c, 
    0x2, 0xef, 0xed, 0x3, 0x2, 0x2, 0x2, 0xf0, 0xf3, 0x3, 0x2, 0x2, 0x2, 
    0xf1, 0xef, 0x3, 0x2, 0x2, 0x2, 0xf1, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf2, 
    0x31, 0x3, 0x2, 0x2, 0x2, 0xf3, 0xf1, 0x3, 0x2, 0x2, 0x2, 0xf4, 0xf5, 
    0x7, 0x17, 0x2, 0x2, 0xf5, 0x33, 0x3, 0x2, 0x2, 0x2, 0xf6, 0xfb, 0x5, 
    0x36, 0x1c, 0x2, 0xf7, 0xf8, 0x7, 0x7, 0x2, 0x2, 0xf8, 0xfa, 0x5, 0x36, 
    0x1c, 0x2, 0xf9, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfd, 0x3, 0x2, 0x2, 
    0x2, 0xfb, 0xf9, 0x3, 0x2, 0x2, 0x2, 0xfb, 0xfc, 0x3, 0x2, 0x2, 0x2, 
    0xfc, 0x35, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xfb, 0x3, 0x2, 0x2, 0x2, 0xfe, 
    0xff, 0x8, 0x1c, 0x1, 0x2, 0xff, 0x109, 0x5, 0x3e, 0x20, 0x2, 0x100, 
    0x109, 0x5, 0x3a, 0x1e, 0x2, 0x101, 0x109, 0x5, 0x32, 0x1a, 0x2, 0x102, 
    0x109, 0x5, 0x40, 0x21, 0x2, 0x103, 0x104, 0x7, 0x8, 0x2, 0x2, 0x104, 
    0x105, 0x5, 0x36, 0x1c, 0x2, 0x105, 0x106, 0x7, 0x9, 0x2, 0x2, 0x106, 
    0x109, 0x3, 0x2, 0x2, 0x2, 0x107, 0x109, 0x5, 0x2a, 0x16, 0x2, 0x108, 
    0xfe, 0x3, 0x2, 0x2, 0x2, 0x108, 0x100, 0x3, 0x2, 0x2, 0x2, 0x108, 0x101, 
    0x3, 0x2, 0x2, 0x2, 0x108, 0x102, 0x3, 0x2, 0x2, 0x2, 0x108, 0x103, 
    0x3, 0x2, 0x2, 0x2, 0x108, 0x107, 0x3, 0x2, 0x2, 0x2, 0x109, 0x114, 
    0x3, 0x2, 0x2, 0x2, 0x10a, 0x10b, 0xc, 0xa, 0x2, 0x2, 0x10b, 0x10c, 
    0x5, 0x42, 0x22, 0x2, 0x10c, 0x10d, 0x5, 0x36, 0x1c, 0xb, 0x10d, 0x113, 
    0x3, 0x2, 0x2, 0x2, 0x10e, 0x10f, 0xc, 0x9, 0x2, 0x2, 0x10f, 0x110, 
    0x5, 0x44, 0x23, 0x2, 0x110, 0x111, 0x5, 0x36, 0x1c, 0xa, 0x111, 0x113, 
    0x3, 0x2, 0x2, 0x2, 0x112, 0x10a, 0x3, 0x2, 0x2, 0x2, 0x112, 0x10e, 
    0x3, 0x2, 0x2, 0x2, 0x113, 0x116, 0x3, 0x2, 0x2, 0x2, 0x114, 0x112, 
    0x3, 0x2, 0x2, 0x2, 0x114, 0x115, 0x3, 0x2, 0x2, 0x2, 0x115, 0x37, 0x3, 
    0x2, 0x2, 0x2, 0x116, 0x114, 0x3, 0x2, 0x2, 0x2, 0x117, 0x118, 0x5, 
    0x36, 0x1c, 0x2, 0x118, 0x119, 0x5, 0x46, 0x24, 0x2, 0x119, 0x11a, 0x5, 
    0x36, 0x1c, 0x2, 0x11a, 0x39, 0x3, 0x2, 0x2, 0x2, 0x11b, 0x11c, 0x5, 
    0x3c, 0x1f, 0x2, 0x11c, 0x11d, 0x5, 0x3e, 0x20, 0x2, 0x11d, 0x3b, 0x3, 
    0x2, 0x2, 0x2, 0x11e, 0x11f, 0x9, 0x2, 0x2, 0x2, 0x11f, 0x3d, 0x3, 0x2, 
    0x2, 0x2, 0x120, 0x123, 0x7, 0x18, 0x2, 0x2, 0x121, 0x123, 0x7, 0x19, 
    0x2, 0x2, 0x122, 0x120, 0x3, 0x2, 0x2, 0x2, 0x122, 0x121, 0x3, 0x2, 
    0x2, 0x2, 0x123, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x124, 0x125, 0x7, 0x1a, 
    0x2, 0x2, 0x125, 0x41, 0x3, 0x2, 0x2, 0x2, 0x126, 0x127, 0x9, 0x3, 0x2, 
    0x2, 0x127, 0x43, 0x3, 0x2, 0x2, 0x2, 0x128, 0x129, 0x9, 0x2, 0x2, 0x2, 
    0x129, 0x45, 0x3, 0x2, 0x2, 0x2, 0x12a, 0x12b, 0x9, 0x4, 0x2, 0x2, 0x12b, 
    0x47, 0x3, 0x2, 0x2, 0x2, 0x19, 0x53, 0x58, 0x5e, 0x6e, 0x7a, 0x85, 
    0x8b, 0x94, 0x9c, 0xa2, 0xae, 0xb8, 0xbf, 0xd1, 0xd6, 0xdd, 0xe6, 0xf1, 
    0xfb, 0x108, 0x112, 0x114, 0x122, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

RatParser::Initializer RatParser::_init;
