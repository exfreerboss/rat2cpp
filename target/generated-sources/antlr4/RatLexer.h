
#include "wci/intermediate/TypeSpec.h"
using namespace wci::intermediate;


// Generated from Rat.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"




class  RatLexer : public antlr4::Lexer {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, TRIGGER = 9, PULLOUT = 10, IDS = 11, ECHO = 12, TIL = 13, 
    SPOSE = 14, USE = 15, OWISE = 16, PROC = 17, FUNC = 18, PRINT = 19, 
    RETURN = 20, IDENTIFIER = 21, INTEGER = 22, FLOAT = 23, STRING = 24, 
    MUL_OP = 25, DIV_OP = 26, ADD_OP = 27, SUB_OP = 28, ASSIGN = 29, EQ_OP = 30, 
    NE_OP = 31, LT_OP = 32, LE_OP = 33, GT_OP = 34, GE_OP = 35, NEWLINE = 36, 
    WS = 37
  };

  RatLexer(antlr4::CharStream *input);
  ~RatLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

