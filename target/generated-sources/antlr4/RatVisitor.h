
#include "wci/intermediate/TypeSpec.h"
using namespace wci::intermediate;


// Generated from Rat.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "RatParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by RatParser.
 */
class  RatVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by RatParser.
   */
    virtual antlrcpp::Any visitProgram(RatParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitHeader(RatParser::HeaderContext *context) = 0;

    virtual antlrcpp::Any visitMainBlock(RatParser::MainBlockContext *context) = 0;

    virtual antlrcpp::Any visitBlock(RatParser::BlockContext *context) = 0;

    virtual antlrcpp::Any visitDeclarations(RatParser::DeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitDecList(RatParser::DecListContext *context) = 0;

    virtual antlrcpp::Any visitDecl(RatParser::DeclContext *context) = 0;

    virtual antlrcpp::Any visitVarList(RatParser::VarListContext *context) = 0;

    virtual antlrcpp::Any visitVarId(RatParser::VarIdContext *context) = 0;

    virtual antlrcpp::Any visitTypeId(RatParser::TypeIdContext *context) = 0;

    virtual antlrcpp::Any visitProcDecl(RatParser::ProcDeclContext *context) = 0;

    virtual antlrcpp::Any visitFuncDecl(RatParser::FuncDeclContext *context) = 0;

    virtual antlrcpp::Any visitMethodId(RatParser::MethodIdContext *context) = 0;

    virtual antlrcpp::Any visitParamList(RatParser::ParamListContext *context) = 0;

    virtual antlrcpp::Any visitStmt(RatParser::StmtContext *context) = 0;

    virtual antlrcpp::Any visitStmtList(RatParser::StmtListContext *context) = 0;

    virtual antlrcpp::Any visitAssignmentStmt(RatParser::AssignmentStmtContext *context) = 0;

    virtual antlrcpp::Any visitEchoStmt(RatParser::EchoStmtContext *context) = 0;

    virtual antlrcpp::Any visitSposeStmt(RatParser::SposeStmtContext *context) = 0;

    virtual antlrcpp::Any visitProcStmt(RatParser::ProcStmtContext *context) = 0;

    virtual antlrcpp::Any visitFuncStmt(RatParser::FuncStmtContext *context) = 0;

    virtual antlrcpp::Any visitPrintStmt(RatParser::PrintStmtContext *context) = 0;

    virtual antlrcpp::Any visitMethodCallId(RatParser::MethodCallIdContext *context) = 0;

    virtual antlrcpp::Any visitValue(RatParser::ValueContext *context) = 0;

    virtual antlrcpp::Any visitVariable(RatParser::VariableContext *context) = 0;

    virtual antlrcpp::Any visitArgumentList(RatParser::ArgumentListContext *context) = 0;

    virtual antlrcpp::Any visitStringExpr(RatParser::StringExprContext *context) = 0;

    virtual antlrcpp::Any visitVariableExpr(RatParser::VariableExprContext *context) = 0;

    virtual antlrcpp::Any visitAddSubExpr(RatParser::AddSubExprContext *context) = 0;

    virtual antlrcpp::Any visitFuncStmtExpr(RatParser::FuncStmtExprContext *context) = 0;

    virtual antlrcpp::Any visitUnsignedNumberExpr(RatParser::UnsignedNumberExprContext *context) = 0;

    virtual antlrcpp::Any visitMulDivExpr(RatParser::MulDivExprContext *context) = 0;

    virtual antlrcpp::Any visitParenExpr(RatParser::ParenExprContext *context) = 0;

    virtual antlrcpp::Any visitSignedNumberExpr(RatParser::SignedNumberExprContext *context) = 0;

    virtual antlrcpp::Any visitRelOpExpr(RatParser::RelOpExprContext *context) = 0;

    virtual antlrcpp::Any visitSignedNumber(RatParser::SignedNumberContext *context) = 0;

    virtual antlrcpp::Any visitSign(RatParser::SignContext *context) = 0;

    virtual antlrcpp::Any visitIntegerConst(RatParser::IntegerConstContext *context) = 0;

    virtual antlrcpp::Any visitFloatConst(RatParser::FloatConstContext *context) = 0;

    virtual antlrcpp::Any visitString(RatParser::StringContext *context) = 0;

    virtual antlrcpp::Any visitMulDivOp(RatParser::MulDivOpContext *context) = 0;

    virtual antlrcpp::Any visitAddSubOp(RatParser::AddSubOpContext *context) = 0;

    virtual antlrcpp::Any visitRelOp(RatParser::RelOpContext *context) = 0;


};

