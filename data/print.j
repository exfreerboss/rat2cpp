.class public print
.super java/lang/Object

.field private static _runTimer LRunTimer;
.field private static _standardIn LPascalTextIn;

; i:nums

.field private static i I

.method public <init>()V

	aload_0
	invokenonvirtual    java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V

	new RunTimer
	dup
	invokenonvirtual RunTimer/<init>()V
	putstatic        print/_runTimer LRunTimer;
	new PascalTextIn
	dup
	invokenonvirtual PascalTextIn/<init>()V
	putstatic        print/_standardIn LPascalTextIn;

; i=10

	ldc	10
	putstatic	print/i I

; PRINT("RAT language says HELLO WORLD! i: %d",i)

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"RAT language says HELLO WORLD! i: %d"
	ldc	1
	anewarray java/lang/Object
	dup
	ldc	0
	getstatic	print/i I
	invokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer;
	aastore
	invokestatic  java/lang/String.format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 


	getstatic     print/_runTimer LRunTimer;
	invokevirtual RunTimer.printElapsedTime()V

	return

.limit locals 16
.limit stack 16
.end method
