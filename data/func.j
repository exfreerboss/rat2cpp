.class public func
.super java/lang/Object

.field private static _runTimer LRunTimer;
.field private static _standardIn LPascalTextIn;

; a:nums

.field private static a I
.field private static foo I

; c:nums

.field private static c I

.method public <init>()V

	aload_0
	invokenonvirtual    java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V

	new RunTimer
	dup
	invokenonvirtual RunTimer/<init>()V
	putstatic        func/_runTimer LRunTimer;
	new PascalTextIn
	dup
	invokenonvirtual PascalTextIn/<init>()V
	putstatic        func/_standardIn LPascalTextIn;
	goto foo_end
foo:
	astore_1

; c=c+1

	getstatic	func/c I
	ldc	1
	iadd
	putstatic	func/c I

; 

	getstatic	func/c I
	ret 1
foo_end:

; a=foo(1)

	ldc	1
	putstatic	func/c I
	jsr foo
	putstatic	func/a I

; PRINT("Hello from main! %d",a)

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"Hello from main! %d"
	ldc	1
	anewarray java/lang/Object
	dup
	ldc	0
	getstatic	func/a I
	invokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer;
	aastore
	invokestatic  java/lang/String.format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 


	getstatic     func/_runTimer LRunTimer;
	invokevirtual RunTimer.printElapsedTime()V

	return

.limit locals 16
.limit stack 16
.end method
