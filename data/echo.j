.class public echo
.super java/lang/Object

.field private static _runTimer LRunTimer;
.field private static _standardIn LPascalTextIn;

; a:nums

.field private static a I

.method public <init>()V

	aload_0
	invokenonvirtual    java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V

	new RunTimer
	dup
	invokenonvirtual RunTimer/<init>()V
	putstatic        echo/_runTimer LRunTimer;
	new PascalTextIn
	dup
	invokenonvirtual PascalTextIn/<init>()V
	putstatic        echo/_standardIn LPascalTextIn;

; a=0

	ldc	0
	putstatic	echo/a I

; ECHOa=a+1.TILa==3

Label_0:

; a=a+1

	getstatic	echo/a I
	ldc	1
	iadd
	putstatic	echo/a I

; 

	getstatic	echo/a I
	ldc	3
	if_icmpeq Label_1
	goto Label_0
Label_1:

; PRINT("a: %d",a)

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"a: %d"
	ldc	1
	anewarray java/lang/Object
	dup
	ldc	0
	getstatic	echo/a I
	invokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer;
	aastore
	invokestatic  java/lang/String.format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 


	getstatic     echo/_runTimer LRunTimer;
	invokevirtual RunTimer.printElapsedTime()V

	return

.limit locals 16
.limit stack 16
.end method
