.class public spose
.super java/lang/Object

.field private static _runTimer LRunTimer;
.field private static _standardIn LPascalTextIn;

; a:nums

.field private static a I

.method public <init>()V

	aload_0
	invokenonvirtual    java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V

	new RunTimer
	dup
	invokenonvirtual RunTimer/<init>()V
	putstatic        spose/_runTimer LRunTimer;
	new PascalTextIn
	dup
	invokenonvirtual PascalTextIn/<init>()V
	putstatic        spose/_standardIn LPascalTextIn;

; a=2

	ldc	2
	putstatic	spose/a I

; SPOSEa==1USEPRINT("a is 1.").OWISEPRINT("a is not 1").

	getstatic	spose/a I
	ldc	1
	if_icmpeq Label_1

; PRINT("a is not 1")

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"a is not 1"
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 

	goto Label_1
Label_0:

; PRINT("a is 1.")

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"a is 1."
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 

	goto Label_1
Label_1:

	getstatic     spose/_runTimer LRunTimer;
	invokevirtual RunTimer.printElapsedTime()V

	return

.limit locals 16
.limit stack 16
.end method
