.class public proc
.super java/lang/Object

.field private static _runTimer LRunTimer;
.field private static _standardIn LPascalTextIn;

; a:nums

.field private static a I

; c:nums

.field private static c I

.method public <init>()V

	aload_0
	invokenonvirtual    java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V

	new RunTimer
	dup
	invokenonvirtual RunTimer/<init>()V
	putstatic        proc/_runTimer LRunTimer;
	new PascalTextIn
	dup
	invokenonvirtual PascalTextIn/<init>()V
	putstatic        proc/_standardIn LPascalTextIn;
	goto foo_end
foo:
	astore_1

; PRINT("Hello from PROC foo! %d",c)

	getstatic java/lang/System/out Ljava/io/PrintStream;
	ldc	"Hello from PROC foo! %d"
	ldc	1
	anewarray java/lang/Object
	dup
	ldc	0
	getstatic	proc/c I
	invokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer;
	aastore
	invokestatic  java/lang/String.format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
	invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

; 

	ret 1
foo_end:

; a=1

	ldc	1
	putstatic	proc/a I

; foo(a)

	getstatic	proc/a I
	putstatic	proc/c I
	jsr foo

; 


	getstatic     proc/_runTimer LRunTimer;
	invokevirtual RunTimer.printElapsedTime()V

	return

.limit locals 16
.limit stack 16
.end method
