/*
 * Rat1Visitor.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: kimflores
 */

#include <Rat1Visitor.h>
#include <iostream>
#include <string>
#include <vector>

#include "wci/intermediate/SymTabFactory.h"
#include "wci/intermediate/symtabimpl/Predefined.h"
#include "wci/util/CrossReferencer.h"

using namespace std;
using namespace wci;
using namespace wci::intermediate;
using namespace wci::intermediate::symtabimpl;
using namespace wci::util;

static string method_name = "";
//static SymTabEntry *variable_id;

static string EXCEPTION(string message)
{
	cout << "ERROR: " << message << endl;
	exit(1);
}

Rat1Visitor::Rat1Visitor()
    : program_id(nullptr), j_file(nullptr)
{
    // Create and initialize the symbol table stack.
    symtab_stack = SymTabFactory::create_symtab_stack();
    Predefined::initialize(symtab_stack);

    //cout << "=== Pass1Visitor(): symtab stack initialized." << endl;
}

Rat1Visitor::~Rat1Visitor() {}

ostream& Rat1Visitor::get_assembly_file() { return j_file; }

antlrcpp::Any Rat1Visitor::visitProgram(RatParser::ProgramContext *ctx)
{
    auto value = visitChildren(ctx);

//    cout << "=== visitProgram: Printing xref table." << endl;

    // Print the cross-reference table.
    CrossReferencer cross_referencer;
    cross_referencer.print(symtab_stack);

    return value;
}

antlrcpp::Any Rat1Visitor::visitHeader(RatParser::HeaderContext *ctx)
{
//    cout << "=== visitHeader: " + ctx->getText() << endl;

    string program_name = ctx->IDENTIFIER()->toString();

    program_id = symtab_stack->enter_local(program_name);
    program_id->set_definition((Definition)DF_PROGRAM);
    program_id->set_attribute((SymTabKey) ROUTINE_SYMTAB,
                              symtab_stack->push());
    symtab_stack->set_program_id(program_id);

    // Create the assembly output file.
    j_file.open(program_name + ".j");
    if (j_file.fail())
    {
            cout << "***** Cannot open assembly file." << endl;
            exit(-99);
    }

    // Emit the program header.
    j_file << ".class public " << program_name << endl;
    j_file << ".super java/lang/Object" << endl;

    // Emit the RunTimer and PascalTextIn fields.
    j_file << endl;
    j_file << ".field private static _runTimer LRunTimer;" << endl;
    j_file << ".field private static _standardIn LPascalTextIn;" << endl;

    return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitBlock(RatParser::BlockContext *ctx)
{
	auto value = visitChildren(ctx);

	// Emit the class constructor.
	j_file << endl;
	j_file << ".method public <init>()V" << endl;
	j_file << endl;
	j_file << "\taload_0" << endl;
	j_file << "\tinvokenonvirtual    java/lang/Object/<init>()V" << endl;
	j_file << "\treturn" << endl;
	j_file << endl;
	j_file << ".limit locals 1" << endl;
	j_file << ".limit stack 1" << endl;
	j_file << ".end method" << endl;

	return value;
}

antlrcpp::Any Rat1Visitor::visitDeclarations(RatParser::DeclarationsContext *ctx)
{
//    cout << "=== visitDeclarations: " << ctx->getText() << endl;

    auto value = visitChildren(ctx);
    variable_id_list.resize(0);

    return value;
}

antlrcpp::Any Rat1Visitor::visitDecl(RatParser::DeclContext *ctx)
{
//    cout << "=== visitDecl: " + ctx->getText() << endl;

    j_file << "\n; " << ctx->getText() << "\n" << endl;
    return visitChildren(ctx);

}

antlrcpp::Any Rat1Visitor::visitVarList(RatParser::VarListContext *ctx)
{
//    cout << "=== visitVarList: " + ctx->getText() << endl;

    variable_id_list.resize(0);
    return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitVarId(RatParser::VarIdContext *ctx)
{
//    cout << "=== visitVarId: " + ctx->getText() << endl;

    string variable_name = ctx->IDENTIFIER()->toString();
    if(symtab_stack->lookup(variable_name) != NULL)
	{
		EXCEPTION("Redeclaration of variable");
	}
    SymTabEntry *variable_id = symtab_stack->enter_local(variable_name);
    variable_id->set_definition((Definition) DF_VARIABLE);
    variable_id_list.push_back(variable_id);

	return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitTypeId(RatParser::TypeIdContext *ctx)
{
    //cout << "=== visitTypeId: " + ctx->getText() << endl;

    TypeSpec *type;
    string type_indicator;

    string type_name = ctx->IDENTIFIER()->toString();
    if (type_name == "nums")
    {
        type = Predefined::integer_type;
        type_indicator = "I";
    }
    else if (type_name == "rnums")
    {
        type = Predefined::real_type;
        type_indicator = "F";
    }
    else if (type_name == "string")
	{
		type = Predefined::char_type;
		type_indicator = "Ljava/lang/String;";
	}
	else
	{
		type = nullptr;
		type_indicator = "?";
		EXCEPTION("Invalid type");
	}

    for (SymTabEntry *id : variable_id_list) {
    	//cout << id->get_name() << " " << type_indicator << endl;
        id->set_typespec(type);

        // Emit a field declaration.
        j_file << ".field private static "
               << id->get_name() << " " << type_indicator << endl;
    }

    return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitMethodId(RatParser::MethodIdContext *ctx)
{
	string method_name = ctx->IDENTIFIER()->toString();

	if(symtab_stack->lookup(method_name) != NULL)
	{
		EXCEPTION("Redeclaration of method");
	}

	SymTabEntry *method_id = symtab_stack->enter_local(method_name);
	method_id->set_definition((Definition) DF_FUNCTION);
	variable_id_list.push_back(method_id);

	return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitProcStmt(RatParser::ProcStmtContext *ctx)
{
	string method_name = ctx->methodCallId()->IDENTIFIER()->toString();
	SymTabEntry *method_id = symtab_stack->lookup(method_name);

	if(method_id == NULL)
	{
		EXCEPTION("Method is not defined");
	}

	return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitProcDecl(RatParser::ProcDeclContext *ctx)
{
	method_name = ctx->methodId()->IDENTIFIER()->getText();

	auto value = visit(ctx->methodId());

	visit(ctx->paramList());
	visit(ctx->stmtList());
	method_name = "";

	return value;

}

antlrcpp::Any Rat1Visitor::visitFuncDecl(RatParser::FuncDeclContext *ctx)
{
	method_name = ctx->methodId()->IDENTIFIER()->getText();

	auto value = visit(ctx->methodId());
	visit(ctx->typeId());

	TypeSpec *type;
	string type_indicator;

	string type_name = ctx->typeId()->getText();
	if (type_name == "nums")
	{
		type = Predefined::integer_type;
		type_indicator = "I";
	}
	else if (type_name == "rnums")
	{
		type = Predefined::real_type;
		type_indicator = "F";
	}
	else if (type_name == "string")
	{
		type = Predefined::char_type;
		type_indicator = "Ljava/lang/String;";
	}
	else
	{
		type = nullptr;
		type_indicator = "?";
		EXCEPTION("Invalid type");
	}

	visit(ctx->paramList());
	visit(ctx->stmtList());
	visit(ctx->expr());
	method_name = "";

	return value;
}

antlrcpp::Any Rat1Visitor::visitAddSubExpr(RatParser::AddSubExprContext *ctx)
{
//    cout << "=== visitAddSubExpr: " + ctx->getText() << endl;

    auto value = visitChildren(ctx);

    TypeSpec *type1 = ctx->expr(0)->type;
    TypeSpec *type2 = ctx->expr(1)->type;

    bool integer_mode =    (type1 == Predefined::integer_type)
                        && (type2 == Predefined::integer_type);
    bool real_mode    =    (type1 == Predefined::real_type)
                        && (type2 == Predefined::real_type);

    TypeSpec *type = integer_mode ? Predefined::integer_type
                   : real_mode    ? Predefined::real_type
                   :                nullptr;
    ctx->type = type;

    return value;
}

antlrcpp::Any Rat1Visitor::visitMulDivExpr(RatParser::MulDivExprContext *ctx)
{
//    cout << "=== visitMulDivExpr: " + ctx->getText() << endl;

    auto value = visitChildren(ctx);

    TypeSpec *type1 = ctx->expr(0)->type;
    TypeSpec *type2 = ctx->expr(1)->type;

    bool integer_mode =    (type1 == Predefined::integer_type)
                        && (type2 == Predefined::integer_type);
    bool real_mode    =    (type1 == Predefined::real_type)
                        && (type2 == Predefined::real_type);

    TypeSpec *type = integer_mode ? Predefined::integer_type
                   : real_mode    ? Predefined::real_type
                   :                nullptr;
    ctx->type = type;

    return value;
}

antlrcpp::Any Rat1Visitor::visitVariableExpr(RatParser::VariableExprContext *ctx)
{
//    cout << "=== visitVariableExpr: " + ctx->getText() << endl;

    string variable_name = ctx->variable()->IDENTIFIER()->toString();
    SymTabEntry *variable_id = symtab_stack->lookup(variable_name);

    ctx->type = variable_id->get_typespec();
    return visitChildren(ctx);

//    auto value =  visitChildren(ctx);
//	ctx->type = ctx->variable()->type;
//
//	return value;
}

antlrcpp::Any Rat1Visitor::visitSignedNumberExpr(RatParser::SignedNumberExprContext *ctx)
{
//    cout << "=== visitSignedNumberExpr: " + ctx->getText() << endl;

    auto value = visitChildren(ctx);
    ctx->type = ctx->signedNumber()->type;
    return value;
}

antlrcpp::Any Rat1Visitor::visitSignedNumber(RatParser::SignedNumberContext *ctx)
{
//    cout << "=== visitSignedNumber: " + ctx->getText() << endl;

    auto value = visit(ctx->number());
    ctx->type = ctx->number()->type;
    return value;
}

antlrcpp::Any Rat1Visitor::visitUnsignedNumberExpr(RatParser::UnsignedNumberExprContext *ctx)
{
//    cout << "=== visitUnsignedNumberExpr: " + ctx->getText() << endl;

    auto value = visit(ctx->number());
    ctx->type = ctx->number()->type;
    return value;
}

antlrcpp::Any Rat1Visitor::visitStringExpr(RatParser::StringExprContext *ctx)
{
	   // cout << "=== visitStringExpr: " + ctx->getText() << endl;

        auto value = visitChildren(ctx);
	    ctx->type = ctx->string()->type;

	    return value;
}

antlrcpp::Any Rat1Visitor::visitIntegerConst(RatParser::IntegerConstContext *ctx)
{
//    cout << "=== visitIntegerConst: " + ctx->getText() << endl;

    ctx->type = Predefined::integer_type;
    return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitFloatConst(RatParser::FloatConstContext *ctx)
{
//    cout << "=== visitFloatConst: " + ctx->getText() << endl;

    ctx->type = Predefined::real_type;
    return visitChildren(ctx);
}

antlrcpp::Any Rat1Visitor::visitParenExpr(RatParser::ParenExprContext *ctx)
{
//    cout << "=== visitParenExpr: " + ctx->getText() << endl;

    auto value = visitChildren(ctx);
    ctx->type = ctx->expr()->type;
    return value;
}

antlrcpp::Any Rat1Visitor::visitFuncStmtExpr(RatParser::FuncStmtExprContext *ctx)
{

	string func_name = ctx->funcStmt()->methodCallId()->IDENTIFIER()->toString();
	SymTabEntry *function_id = symtab_stack->lookup(func_name);

	if(function_id == NULL)
	{
		EXCEPTION("Function is not defined");
	}
	ctx->type = function_id->get_typespec();

	return visitChildren(ctx);
}



