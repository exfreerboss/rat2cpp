/*
 * Rat2Visitor.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: kimflores
 */

#include <Rat2Visitor.h>
#include <iostream>
#include <string>

#include "wci/intermediate/SymTabStack.h"
#include "wci/intermediate/SymTabEntry.h"
#include "wci/intermediate/TypeSpec.h"
#include "wci/intermediate/symtabimpl/Predefined.h"

using namespace wci;
using namespace wci::intermediate;
using namespace wci::intermediate::symtabimpl;

//extern string program_name;
int label_counter = 0;
string proc_name = "";
string func_name = "";
string proc_typeID = "";
string func_typeID = "";

static unordered_map<string, vector<vector<string>>> method_param_map;

static string EXCEPTION(string message)
{
	cout << "Error: " << message << endl;
	exit(1);
}


Rat2Visitor::Rat2Visitor(ostream& j_file)
    : program_name(""), j_file(j_file)
{
}

Rat2Visitor::~Rat2Visitor() {}

antlrcpp::Any Rat2Visitor::visitProgram(RatParser::ProgramContext *ctx)
{
    auto value = visitChildren(ctx);
    //j_file.close();
    return value;
}

antlrcpp::Any Rat2Visitor::visitHeader(RatParser::HeaderContext *ctx)
{
    program_name = ctx->IDENTIFIER()->toString();
    return visitChildren(ctx);
}

antlrcpp::Any Rat2Visitor::visitMainBlock(RatParser::MainBlockContext *ctx)
{
    // Emit the main program header.
    j_file << endl;
    j_file << ".method public static main([Ljava/lang/String;)V" << endl;
    j_file << endl;
    j_file << "\tnew RunTimer" << endl;
    j_file << "\tdup" << endl;
    j_file << "\tinvokenonvirtual RunTimer/<init>()V" << endl;
    j_file << "\tputstatic        " << program_name
           << "/_runTimer LRunTimer;" << endl;
    j_file << "\tnew PascalTextIn" << endl;
    j_file << "\tdup" << endl;
    j_file << "\tinvokenonvirtual PascalTextIn/<init>()V" << endl;
    j_file << "\tputstatic        " + program_name
           << "/_standardIn LPascalTextIn;" << endl;

    auto value = visitChildren(ctx);

    // Emit the main program epilogue.
    j_file << endl;
    j_file << "\tgetstatic     " << program_name
               << "/_runTimer LRunTimer;" << endl;
    j_file << "\tinvokevirtual RunTimer.printElapsedTime()V" << endl;
    j_file << endl;
    j_file << "\treturn" << endl;
    j_file << endl;
    j_file << ".limit locals 16" << endl;
    j_file << ".limit stack 16" << endl;
    j_file << ".end method" << endl;

    return value;
}

antlrcpp::Any Rat2Visitor::visitStmt(RatParser::StmtContext *ctx)
{
    j_file << endl << "; " + ctx->getText() << endl << endl;

    return visitChildren(ctx);
}

antlrcpp::Any Rat2Visitor::visitProcDecl(RatParser::ProcDeclContext *ctx)
{
	proc_name = ctx->methodId()->IDENTIFIER()->getText();

	j_file << "\tgoto " << proc_name << "_end" << endl;

	j_file << proc_name << ":" << endl;

	j_file << "\tastore_1" << endl;

	vector<vector<string>> params;

	 if(ctx->paramList() != NULL)
	 {
		 for(int i = 0; i < ctx->paramList()->decl().size(); i++)
		 {
			 string type_name     = ctx->paramList()->decl(i)->typeId()->getText();
			 string variable_name = ctx->paramList()->decl(i)->varList()->getText();

			 params.push_back({variable_name, type_name});
		 }
	 }

	 method_param_map.emplace(ctx->methodId()->IDENTIFIER()->getText(), params);

	 auto value = visitChildren(ctx->stmtList());

	 j_file << "\tret 1" << endl;
	 j_file << proc_name << "_end:" << endl;
	 proc_name = "";

	 return value;
}

antlrcpp::Any Rat2Visitor::visitFuncDecl(RatParser::FuncDeclContext *ctx)
{
	func_name = ctx->methodId()->getText();

	j_file << "\tgoto " << func_name << "_end" << endl;

	j_file << func_name << ":" << endl;

	j_file << "\tastore_1" << endl;

	vector<vector<string>> params;

	if(ctx->paramList() != NULL)
	{
		for(int i = 0; i < ctx->paramList()->decl().size(); i++)
		{
			string type_name     = ctx->paramList()->decl(i)->typeId()->getText();
			string variable_name = ctx->paramList()->decl(i)->varList()->getText();

			params.push_back({variable_name, type_name});
		}
	}

	method_param_map.emplace(ctx->methodId()->IDENTIFIER()->getText(), params);

	auto value = visitChildren(ctx->stmtList());

	if(ctx->expr() != NULL)
	{
		visit(ctx->expr());
	}

	j_file << "\tret 1" << endl;
	j_file << func_name << "_end:" << endl;
	func_name = "";

	return value;
}

antlrcpp::Any Rat2Visitor::visitAssignmentStmt(RatParser::AssignmentStmtContext *ctx)
{
    auto value = visit(ctx->expr());

    string type_indicator =
                  (ctx->expr()->type == Predefined::integer_type) ? "I"
                : (ctx->expr()->type == Predefined::real_type)    ? "F"
				: (ctx->expr()->type == Predefined::char_type)    ? "Ljava/lang/String;"
				:                                                         EXCEPTION("Invalid type");


    // Emit a field put instruction.
    j_file << "\tputstatic\t" << program_name
           << "/" << ctx->variable()->IDENTIFIER()->toString()
           << " " << type_indicator << endl;

    return value;
}

antlrcpp::Any Rat2Visitor::visitEchoStmt(RatParser::EchoStmtContext *ctx)
{
	int original_label = label_counter;

	j_file << "Label_" << original_label << ":" << endl;

	visitChildren(ctx->stmtList());

	visit(ctx->relOpExpr());

	j_file << "\tgoto Label_" << original_label << endl;

	j_file << "Label_" << label_counter << ":" << endl;

	label_counter++;

	return NULL;
}

// if statement
antlrcpp::Any Rat2Visitor::visitSposeStmt(RatParser::SposeStmtContext *ctx)
{

	int label_num = label_counter;
	int stmt_size = ctx->stmtList().size();

	// if there are more than 1 stmtLists, there is an OWISE
	bool has_owise = (stmt_size > 1)? true : false ;

	visit(ctx->relOpExpr());

	if(has_owise)
	{
		visitChildren(ctx->stmtList(stmt_size-1));
	}

	j_file << "\tgoto " << "Label_" << label_num+1 << endl;
	j_file << "Label_" << label_num << ":" << endl;
	visitChildren(ctx->stmtList(0));
	j_file << "\tgoto " << "Label_" << label_num+1 << endl;

	j_file << "Label_" << label_num+1  << ":" << endl;

	label_counter++;
	return NULL;
}

antlrcpp::Any Rat2Visitor::visitProcStmt(RatParser::ProcStmtContext *ctx)
{
	if(ctx->argumentList() != NULL)
	{
		int input_count = ctx->argumentList()->expr().size();

		vector<vector<string>> params = method_param_map.find(ctx->methodCallId()->IDENTIFIER()->getText())->second;

		int params_count = params.size();

		int max = (params_count > input_count) ? input_count: params_count;
		for(int i = 0; i < max; i++)
		{
			string variable_name = params[i][0];
			string type_name     = params[i][1];
			visit(ctx->argumentList()->expr(i));

			string type_indicator =
					  (type_name == "nums")     ? "I"
					: (type_name == "rnums")    ? "F"
					: (type_name == "string")  ? "Ljava/lang/String;"
					:                         EXCEPTION("Invalid type");

			// Emit a field put instruction.
			j_file << "\tputstatic\t" << program_name
			   << "/" << proc_name <<  variable_name
			   << " " << type_indicator << endl;
		}
	}

	j_file << "\tjsr " << ctx->methodCallId()->IDENTIFIER()->getText() << endl;


	return NULL;
}

antlrcpp::Any Rat2Visitor::visitPrintStmt(RatParser::PrintStmtContext *ctx)
{

    j_file << "\tgetstatic java/lang/System/out Ljava/io/PrintStream;" << endl;
    auto value = visit(ctx->string());
    string str = ctx->string()->getText();
    j_file << "\tldc\t" << str << endl;

    if(ctx->value() != NULL)
        {
        	int identifier_count = ctx->value()->expr().size();

        	j_file << "\tldc\t" << identifier_count << endl;
        	j_file << "\tanewarray java/lang/Object" << endl;

        	for(int i = 0; i < identifier_count; i++)
        	{
        		j_file << "\tdup" << endl;
        		j_file << "\tldc\t" << i << endl;
        		visit(ctx->value()->expr(i));
        		TypeSpec *type = ctx->value()->expr(i)->type;
        		if(type == Predefined::integer_type || type == Predefined::real_type)
        		{
        			j_file << "\tinvokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer;" << endl;
        		}

        		j_file << "\taastore" << endl;
        	}

        	j_file << "\tinvokestatic  java/lang/String.format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;" << endl;
        }

	j_file << "\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V" << endl;
	return value;
}

antlrcpp::Any Rat2Visitor::visitFuncStmtExpr(RatParser::FuncStmtExprContext *ctx)
{
	func_name = ctx->funcStmt()->methodCallId()->getText();



	if(ctx->funcStmt()->argumentList() != NULL)
	{
		int input_count = ctx->funcStmt()->argumentList()->expr().size();

		vector<vector<string>> params = method_param_map.find(ctx->funcStmt()->methodCallId()->IDENTIFIER()->getText())->second;

		int params_count = params.size();

		int max = (params_count > input_count) ? input_count: params_count;
		for(int i = 0; i < max; i++)
		{
			string variable_name = params[i][0];
			string type_name     = params[i][1];
			visit(ctx->funcStmt()->argumentList()->expr(i));

			string type_indicator =
					  (type_name == "nums")     ? "I"
					: (type_name == "rnums")    ? "F"
					: (type_name == "string")  ? "Ljava/lang/String;"
					:                         EXCEPTION("Invalid type");

			// Emit a field put instruction.
			j_file << "\tputstatic\t" << program_name
			   << "/" << variable_name << " "
			   << type_indicator << endl;
		}
	}

	j_file << "\tjsr " << func_name << endl;
	return NULL;
}

antlrcpp::Any Rat2Visitor::visitAddSubExpr(RatParser::AddSubExprContext *ctx)
{
    auto value = visitChildren(ctx);

    TypeSpec *type1 = ctx->expr(0)->type;
    TypeSpec *type2 = ctx->expr(1)->type;

    bool integer_mode =    (type1 == Predefined::integer_type)
                        && (type2 == Predefined::integer_type);
    bool real_mode    =    (type1 == Predefined::real_type)
                        && (type2 == Predefined::real_type);

    string op = ctx->addSubOp()->getText();
    string opcode;

    if (op == "+")
    {
        opcode = integer_mode ? "iadd"
               : real_mode    ? "fadd"
               :                "????";
    }
    else
    {
        opcode = integer_mode ? "isub"
               : real_mode    ? "fsub"
               :                "????";
    }

    // Emit an add or subtract instruction.
    j_file << "\t" << opcode << endl;

    return value;
}

antlrcpp::Any Rat2Visitor::visitMulDivExpr(RatParser::MulDivExprContext *ctx)
{
    auto value = visitChildren(ctx);

    TypeSpec *type1 = ctx->expr(0)->type;
    TypeSpec *type2 = ctx->expr(1)->type;

    bool integer_mode =    (type1 == Predefined::integer_type)
                        && (type2 == Predefined::integer_type);
    bool real_mode    =    (type1 == Predefined::real_type)
                        && (type2 == Predefined::real_type);

    string op = ctx->mulDivOp()->getText();
    string opcode;

    if (op == "*")
    {
        opcode = integer_mode ? "imul"
               : real_mode    ? "fmul"
               :                "????";
    }
    else
    {
        opcode = integer_mode ? "idpv"
               : real_mode    ? "fdiv"
               :                "????";
    }

    // Emit a multiply or divide instruction.
    j_file << "\t" << opcode << endl;

    return value;
}

antlrcpp::Any Rat2Visitor::visitVariableExpr(RatParser::VariableExprContext *ctx)
{
    string variable_name = ctx->variable()->IDENTIFIER()->toString();
    TypeSpec *type = ctx->type;

    string type_indicator = (type == Predefined::integer_type) ? "I"
                          : (type == Predefined::real_type)    ? "F"
                          :                                      "?";

    // Emit a field get instruction.
    j_file << "\tgetstatic\t" << program_name
           << "/" << variable_name << " " << type_indicator << endl;

    return visitChildren(ctx);
}

antlrcpp::Any Rat2Visitor::visitRelOpExpr(RatParser::RelOpExprContext *ctx) {
	auto value = visitChildren(ctx);

	TypeSpec *type1 = ctx->expr(0)->type;
	TypeSpec *type2 = ctx->expr(1)->type;

	bool integer_mode =    (type1 == Predefined::integer_type)
						&& (type2 == Predefined::integer_type);
	bool real_mode =       (type1 == Predefined::real_type)
						&& (type2 == Predefined::real_type);

	string op = ctx->relOp()->getText();
	string instruction;

	if (op == "==")
		instruction = "if_icmpeq";
	else if (op == "!=")
		instruction = "if_icmpne";
	else if (op == "<")
		instruction = "if_icmplt";
	else if (op == "<=")
		instruction = "if_icmple";
	else if (op == ">")
		instruction = "if_icmpgt";
	else if (op == ">=")
		instruction = "if_icmpge";
	else
		instruction = "???";


	// Emit a branch instruction.
	j_file << "\t" << instruction << " Label_" << ++label_counter << endl;

	return value;
}

antlrcpp::Any Rat2Visitor::visitSignedNumber(RatParser::SignedNumberContext *ctx)
{
    auto value = visitChildren(ctx);
    TypeSpec *type = ctx->number()->type;

    if (ctx->sign()->children[0] == ctx->sign()->SUB_OP())
    {
        string opcode = (type == Predefined::integer_type) ? "ineg"
                      : (type == Predefined::real_type)    ? "fneg"
                      :                                      "?neg";

        // Emit a negate instruction.
        j_file << "\t" << opcode << endl;
    }

    return value;
}

antlrcpp::Any Rat2Visitor::visitIntegerConst(RatParser::IntegerConstContext *ctx)
{
    // Emit a load constant instruction.
    j_file << "\tldc\t" << ctx->getText() << endl;

    return visitChildren(ctx);
}

antlrcpp::Any Rat2Visitor::visitFloatConst(RatParser::FloatConstContext *ctx)
{
    // Emit a load constant instruction.
    j_file << "\tldc\t" << ctx->getText() << endl;

    return visitChildren(ctx);
}



