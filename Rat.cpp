#include <Rat1Visitor.h>
#include <Rat2Visitor.h>
#include <iostream>
#include <fstream>

#include "antlr4-runtime.h"
#include "RatLexer.h"
#include "RatParser.h"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

int main(int argc, const char *args[])
{
    ifstream ins;
    ins.open(args[1]);

    ANTLRInputStream input(ins);
    RatLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    RatParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    // RatBaseVisitor compiler;
    // compiler.visit(tree);

    Rat1Visitor *rat1 = new Rat1Visitor();
    rat1->visit(tree);

    ostream& j_file = rat1->get_assembly_file();

    Rat2Visitor *rat2 = new Rat2Visitor(j_file);
    rat2->visit(tree);

    // delete tree;
    return 0;
}
