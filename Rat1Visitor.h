/*
 * Rat1Visitor.h
 *
 *  Created on: Apr 30, 2019
 *      Author: kimflores
 */

#ifndef RAT1VISITOR_H_
#define RAT1VISITOR_H_

#include <Rat2Visitor.h>
#include <iostream>

#include "wci/intermediate/SymTabStack.h"
#include "wci/intermediate/SymTabEntry.h"
#include "wci/intermediate/TypeSpec.h"

#include "RatBaseVisitor.h"
#include "antlr4-runtime.h"

using namespace wci;
using namespace wci::intermediate;

class Rat1Visitor : public RatBaseVisitor
{
private:
    SymTabStack *symtab_stack;
    SymTabEntry *program_id;
    vector<SymTabEntry *> variable_id_list;
    ofstream j_file;

public:
    Rat1Visitor();
    virtual ~Rat1Visitor();

    ostream& get_assembly_file();

    antlrcpp::Any visitProgram(RatParser::ProgramContext *ctx) override;
    antlrcpp::Any visitHeader(RatParser::HeaderContext *ctx) override;
    antlrcpp::Any visitBlock(RatParser::BlockContext *ctx) override;
    antlrcpp::Any visitDeclarations(RatParser::DeclarationsContext *ctx) override;
    antlrcpp::Any visitDecl(RatParser::DeclContext *ctx) override;
    antlrcpp::Any visitVarList(RatParser::VarListContext *ctx) override;
    antlrcpp::Any visitVarId(RatParser::VarIdContext *ctx) override;
    antlrcpp::Any visitTypeId(RatParser::TypeIdContext *ctx) override;
    antlrcpp::Any visitMethodId(RatParser::MethodIdContext *ctx) override;
    antlrcpp::Any visitProcStmt(RatParser::ProcStmtContext *ctx) override;
    antlrcpp::Any visitProcDecl(RatParser::ProcDeclContext *ctx) override;
    antlrcpp::Any visitFuncDecl(RatParser::FuncDeclContext *ctx) override;
    antlrcpp::Any visitAddSubExpr(RatParser::AddSubExprContext *ctx) override;
    antlrcpp::Any visitMulDivExpr(RatParser::MulDivExprContext *ctx) override;
    antlrcpp::Any visitVariableExpr(RatParser::VariableExprContext *ctx) override;
    antlrcpp::Any visitSignedNumberExpr(RatParser::SignedNumberExprContext *ctx) override;
    antlrcpp::Any visitSignedNumber(RatParser::SignedNumberContext *ctx) override;
    antlrcpp::Any visitUnsignedNumberExpr(RatParser::UnsignedNumberExprContext *ctx) override;
    antlrcpp::Any visitStringExpr(RatParser::StringExprContext *ctx) override;
    antlrcpp::Any visitIntegerConst(RatParser::IntegerConstContext *ctx) override;
    antlrcpp::Any visitFloatConst(RatParser::FloatConstContext *ctx) override;
    antlrcpp::Any visitParenExpr(RatParser::ParenExprContext *ctx) override;
    antlrcpp::Any visitFuncStmtExpr(RatParser::FuncStmtExprContext *ctx) override;
};

#endif /* RAT1VISITOR_H_ */
