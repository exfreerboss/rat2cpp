/*
 * Rat2Visitor.h
 *
 *  Created on: Apr 30, 2019
 *      Author: kimflores
 */

#ifndef RAT2VISITOR_H_
#define RAT2VISITOR_H_

#include <Rat2Visitor.h>
#include <iostream>
#include <string>

#include "wci/intermediate/SymTabStack.h"
#include "wci/intermediate/SymTabEntry.h"
#include "wci/intermediate/TypeSpec.h"

#include "RatBaseVisitor.h"
#include "antlr4-runtime.h"

using namespace wci;
using namespace wci::intermediate;

class Rat2Visitor : public RatBaseVisitor
{
private:
	string program_name;
	ostream& j_file;

public:
	Rat2Visitor(ostream& j_file);
    virtual ~Rat2Visitor();

    antlrcpp::Any visitProgram(RatParser::ProgramContext *ctx) override;
    antlrcpp::Any visitHeader(RatParser::HeaderContext *ctx) override;
    antlrcpp::Any visitMainBlock(RatParser::MainBlockContext *ctx) override;
    antlrcpp::Any visitStmt(RatParser::StmtContext *ctx) override;
    antlrcpp::Any visitProcDecl(RatParser::ProcDeclContext *ctx) override;
    antlrcpp::Any visitFuncDecl(RatParser::FuncDeclContext *ctx) override;
    antlrcpp::Any visitAssignmentStmt(RatParser::AssignmentStmtContext *ctx) override;
    antlrcpp::Any visitEchoStmt(RatParser::EchoStmtContext *ctx) override;			// while statement
    antlrcpp::Any visitSposeStmt(RatParser::SposeStmtContext *ctx) override;			// if statement
    antlrcpp::Any visitProcStmt(RatParser::ProcStmtContext *ctx) override;
    antlrcpp::Any visitPrintStmt(RatParser::PrintStmtContext *ctx) override;
    antlrcpp::Any visitFuncStmtExpr(RatParser::FuncStmtExprContext *ctx) override;
    antlrcpp::Any visitAddSubExpr(RatParser::AddSubExprContext *ctx) override;
    antlrcpp::Any visitMulDivExpr(RatParser::MulDivExprContext *ctx) override;
    antlrcpp::Any visitVariableExpr(RatParser::VariableExprContext *ctx) override;
    antlrcpp::Any visitRelOpExpr(RatParser::RelOpExprContext *ctx) override;
    antlrcpp::Any visitSignedNumber(RatParser::SignedNumberContext *ctx) override;
    antlrcpp::Any visitIntegerConst(RatParser::IntegerConstContext *ctx) override;
    antlrcpp::Any visitFloatConst(RatParser::FloatConstContext *ctx) override;
};


#endif /* RAT2VISITOR_H_ */
