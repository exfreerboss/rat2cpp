grammar Rat;  // Ratchet language

@header {
#include "wci/intermediate/TypeSpec.h"
using namespace wci::intermediate;
}

program : header mainBlock '?' ;
header  : '_' IDENTIFIER '.' ;
mainBlock : block ;
block   : (declarations)? ( procDecl )* ( funcDecl )* TRIGGER stmtList PULLOUT;

declarations : IDS decList '.' ;
decList    : decl ( '.' decl )* ;
decl        : varList ':' typeId ;
varList     : varId ( ',' varId )* ;
varId       : IDENTIFIER ;
typeId      : IDENTIFIER ;

procDecl		: PROC methodId '(' (paramList)? ')' '.' TRIGGER (stmtList)? PULLOUT '.' ;
funcDecl		: FUNC methodId '(' (paramList)? ')' ':' typeId '.'
					TRIGGER (stmtList)? (RETURN expr '.')? PULLOUT '.' ;

methodId : IDENTIFIER ;
paramList : decl (';' decl)* ;

stmt : declarations    
     | assignmentStmt  
     | echoStmt      	
     | sposeStmt       
     | procStmt		
  	 | printStmt		
     |                  
     ;
     
stmtList        : stmt ( '.' stmt )* ;
assignmentStmt  : variable ASSIGN expr ;
echoStmt     	: ECHO stmtList TIL relOpExpr ;
sposeStmt       : SPOSE relOpExpr USE stmtList ( OWISE stmtList )? ;
procStmt		: methodCallId '(' (argumentList)? ')' ;
funcStmt		: methodCallId '(' (argumentList)? ')' ;
printStmt		: PRINT '(' string (',' value)? ')' ;

methodCallId : IDENTIFIER ;
value  : expr  (',' expr)*  ;
variable locals [ TypeSpec* type = nullptr, int size = 0 ] : IDENTIFIER ;
argumentList : expr ( ',' expr )* ;

expr locals [ TypeSpec *type = nullptr ]
	 : expr mulDivOp expr     # mulDivExpr
     | expr addSubOp expr     # addSubExpr
     | number                 # unsignedNumberExpr
     | signedNumber			  # signedNumberExpr
     | variable               # variableExpr
     | string				  # stringExpr
     | '(' expr ')'           # parenExpr
     | funcStmt				  # funcStmtExpr
     ;
     
relOpExpr locals [ TypeSpec* type = nullptr ]
	: expr relOp expr
	;    
     
signedNumber locals [ TypeSpec *type = nullptr ] 
    : sign number 
    ;
    
sign : ADD_OP | SUB_OP ;
     
number locals [ TypeSpec *type = nullptr ]
    : INTEGER    # integerConst
    | FLOAT      # floatConst
    ;
    
string locals [ TypeSpec* type = nullptr ]
	: STRING     
	;
     
mulDivOp : MUL_OP | DIV_OP ;
addSubOp : ADD_OP | SUB_OP ;
relOp     : EQ_OP | NE_OP | LT_OP | LE_OP | GT_OP | GE_OP ;

TRIGGER : 'TRIGGER' ;
PULLOUT : 'PULLOUT' ;
IDS     : 'IDS' 	;
ECHO  	: 'ECHO' 	;
TIL   	: 'TIL' 	;
SPOSE   : 'SPOSE' 	;
USE    	: 'USE' 	;
OWISE   : 'OWISE'	;
PROC	: 'PROC'	;
FUNC	: 'FUNC'	;
PRINT	: 'PRINT'	;
RETURN	: 'RETURN'	;

IDENTIFIER : [a-zA-Z][a-zA-Z0-9]* ;
INTEGER    : [0-9]+ ;
FLOAT      : [0-9]+ '.' [0-9]+ ;
STRING	   : '"' .*? '"' ;

MUL_OP :   '*' ;
DIV_OP :   '/' ;
ADD_OP :   '+' ;
SUB_OP :   '-' ;

ASSIGN : '=' ;

EQ_OP : '==' ;
NE_OP : '<>' ;
LT_OP : '<'  ;
LE_OP : '<=' ;
GT_OP : '>' ;
GE_OP : '>=' ;

NEWLINE : '\r'? '\n' -> skip  ;
WS      : [ \t]+ -> skip ; 